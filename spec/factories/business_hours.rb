# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :business_hour do
    place_id 1
    day 1
    start_time "2013-11-27 12:59:44"
    end_time "2013-11-27 12:59:44"
    closed false
  end
end
