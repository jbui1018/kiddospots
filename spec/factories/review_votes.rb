# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :review_vote do
    review_id 1
    user_id 1
    value 1
  end
end
