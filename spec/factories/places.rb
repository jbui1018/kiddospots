require 'faker' 

FactoryGirl.define do
	factory :place do |p|
		p.name { Faker::Company.bs }
		p.category_id 3
		p.feature_ids [13, 14, 17]
		p.description { Faker::Lorem.characters }
		p.price Random.new.rand(0..3) 
		p.age_group_ids [ Random.new.rand(1..2),3, Random.new.rand(4..5) ]
		p.street { Faker::Address.street_address }
		p.city { Faker::Address.city }
		p.zip_code "#{ Faker::Address.zip_code }".to_i
		p.state { Faker::Address.state_abbr }
		p.phone_num "#{ Faker::PhoneNumber.phone_number }".to_i
		p.website "http://www.#{ Faker::Internet.domain_name }"
		association :user
	end
end