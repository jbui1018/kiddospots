require 'faker' 

FactoryGirl.define do
	factory :review do |r|
		association :user
		association :place
		r.body { Faker::Lorem.sentence 100 }
		r.rating Random.new.rand(0.5..5.0).round(2)
	end
end
