FactoryGirl.define do
	factory :user do |u|
		u.name { Faker::Name.name }
		u.email { Faker::Internet.email }
		u.password "password"
		u.password_confirmation "password"
	end
end