require 'spec_helper'

describe "Reviews API" do
	describe "GET Review" do
	  it "should get v1" do
	    get '/api/reviews/1', {}, {'Accept' => 'application/vnd.joeysearch.v1'}
	      assert_response 200
	   end
	end

	describe "POST vote" do
		it "should add vote" do
			post '/api/reviews/1/vote', {:value => 1}, {'Accept' => 'application/vnd.joeysearch.v1'}
		      assert_response 200
		   end
		end
end