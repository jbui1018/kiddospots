require 'spec_helper'

describe "Sessions API" do
	describe "Login" do
	  it "should log in with email and password and return auth_token, email & name" do
	    post '/api/sessions/', "email=bharatkrishna.r@gmail.com&password=bharat", {'Accept' => 'application/vnd.joeysearch.v1'}
	      expect(response).to be_success 
	      expect(json['success']).to eq(true)
	      expect(json['email']).to eq("bharatkrishna.r@gmail.com")
	      expect(json['name']).to eq("Bharat Krishna")
	      expect(json['auth_token']).not_to eq(nil)
	      expect(json['auth_token']).not_to eq("")
	   end
	end
end