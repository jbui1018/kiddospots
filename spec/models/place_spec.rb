require 'spec_helper'

describe Place do
	it "has a valid factory" do
		expect(create(:place)).to be_valid
	end

	context "Place Name" do 
		it "is invalid without a name" do
			expect(build(:place, name: nil)).to_not be_valid 
		end		
	end

	context "Age Group" do
		it "is invalid without an age group" do 
			expect(build(:place, age_group_ids: nil)).to_not be_valid 
		end
		
		it "is valid with only one group selected" do
			expect(build(:place, age_group_ids: [Random.new.rand(1..5)])).to be_valid 
		end

		it "is valid with all age groups selected" do 
			expect(build(:place, age_group_ids: [1, 2, 3, 4, 5])).to be_valid 
		end
	end

	context "Address" do
		it "is invalid with non numeric zip code" do
			expect(build(:place, zip_code: "blah")).to_not be_valid 
		end		
	end

	context "Features" do
		it "is valid with all features selected" do
			expect(build(:place, category_id: 1, feature_ids: (1..9).to_a )).to be_valid 
		end

		it "is valid with no features selected" do
			expect(build(:place, category_id: 1, feature_ids: [])).to be_valid 
		end
	end
end
