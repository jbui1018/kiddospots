require 'spec_helper'

describe "places/show" do
  before(:each) do
    @place = assign(:place, stub_model(Place,
      :name => "Name",
      :rating => 1,
      :category_id => 2,
      :description => "MyText",
      :ages => 3,
      :price => 4,
      :hours => "Hours",
      :address => "MyText",
      :phone_num => "Phone Num",
      :website => "Website",
      :images => "Images"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/MyText/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/Hours/)
    rendered.should match(/MyText/)
    rendered.should match(/Phone Num/)
    rendered.should match(/Website/)
    rendered.should match(/Images/)
  end
end
