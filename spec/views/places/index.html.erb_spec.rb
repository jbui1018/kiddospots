require 'spec_helper'

describe "places/index" do
  before(:each) do
    assign(:places, [
      stub_model(Place,
        :name => "Name",
        :rating => 1,
        :category_id => 2,
        :description => "MyText",
        :ages => 3,
        :price => 4,
        :hours => "Hours",
        :address => "MyText",
        :phone_num => "Phone Num",
        :website => "Website",
        :images => "Images"
      ),
      stub_model(Place,
        :name => "Name",
        :rating => 1,
        :category_id => 2,
        :description => "MyText",
        :ages => 3,
        :price => 4,
        :hours => "Hours",
        :address => "MyText",
        :phone_num => "Phone Num",
        :website => "Website",
        :images => "Images"
      )
    ])
  end

  it "renders a list of places" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "Hours".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Phone Num".to_s, :count => 2
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "Images".to_s, :count => 2
  end
end
