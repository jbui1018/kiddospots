require 'spec_helper'

describe "places/new" do
  before(:each) do
    assign(:place, stub_model(Place,
      :name => "MyString",
      :rating => 1,
      :category_id => 1,
      :description => "MyText",
      :ages => 1,
      :price => 1,
      :hours => "MyString",
      :address => "MyText",
      :phone_num => "MyString",
      :website => "MyString",
      :images => "MyString"
    ).as_new_record)
  end

  it "renders new place form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", places_path, "post" do
      assert_select "input#place_name[name=?]", "place[name]"
      assert_select "input#place_rating[name=?]", "place[rating]"
      assert_select "input#place_category_id[name=?]", "place[category_id]"
      assert_select "textarea#place_description[name=?]", "place[description]"
      assert_select "input#place_ages[name=?]", "place[ages]"
      assert_select "input#place_price[name=?]", "place[price]"
      assert_select "input#place_hours[name=?]", "place[hours]"
      assert_select "textarea#place_address[name=?]", "place[address]"
      assert_select "input#place_phone_num[name=?]", "place[phone_num]"
      assert_select "input#place_website[name=?]", "place[website]"
      assert_select "input#place_images[name=?]", "place[images]"
    end
  end
end
