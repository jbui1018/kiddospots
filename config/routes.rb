require 'api_constraints'

Kiddospots::Application.routes.draw do
  resources :events

  #Android Beta
  resources :android_beta_testers

  #Event Suggestions
  resources :event_suggestions

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}, 
             :controllers => { :omniauth_callbacks => "omniauth_callbacks", :registrations => "registrations" }
  
  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      resources :places do
        resources :photos 
        match 'photos/upload', to: 'photos#upload', via: :post
        match 'user_reviewed', to: 'reviews#user_reviewed', via: :post
        resources :reviews do
          match 'vote', to: 'reviews#vote', via: :post
        end
      end      

      match 'values/categories', to: 'values#categories', via: :get
      match 'values/features', to: 'values#features', via: :get
      match 'values/age_groups', to: 'values#age_groups', via: :get

      devise_scope :user do
        post 'registrations' => 'registrations#create', :as => 'register'
        patch 'registrations' => 'registrations#update', :as => 'user_update'
        post 'sessions' => 'sessions#create', :as => 'login'
        delete 'sessions' => 'sessions#destroy', :as => 'logout'
      end
        
    end
  end

  resources :photos
  resources :reviews do
    member { post :vote }
  end

  resources :places do
      member { post :vote }
  end

  resources :places do
    resources :reviews
    resources :photos
  end
  resources :users

 devise_scope :user do 
      match '/sessions/user', to: 'devise/sessions#create', via: :post
  end

  apipie
  root  'welcome#index'
  match '/about', to: 'static_pages#about', via: 'get'
  match '/help', to: 'static_pages#help', via: 'get'
  match '/auth/:provider/callback', to: 'sessions#createfb', via: 'get'

  #Android Beta
  match '/android_beta', to: 'android_beta_testers#new', via: 'get'
  
  #match '/auth/failure', to: 'sessions#failure', via: 'get'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
