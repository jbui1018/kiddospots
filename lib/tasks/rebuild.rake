namespace :db do

  desc "Rebuild"
  task :rebuild => [:drop, :create, :migrate, :seed, :create_users, :create_places, :create_reviews, :environment] do
  	puts "Database Rebuilt"
  end
end

