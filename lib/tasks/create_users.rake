namespace :db do

  desc "Create users"
  task :create_users => [:migrate, :seed, :environment] do
    User.create({
            :name => "Bharat Krishna",
            :email => "bharatkrishna.r@gmail.com",
            :password => "bharat",
            :password_confirmation => "bharat"
    })
    puts "Created User: email: bharatkrishna.r@gmail.com, password: bharat"

    User.create({
          :name => "Joseph Bui",
          :email => "jbui@hotmail.com",
          :password => "joseph",
          :password_confirmation => "joseph"
    })
    puts "Created User: email: jbui@hotmail.com, password: joseph"

  end
  
end