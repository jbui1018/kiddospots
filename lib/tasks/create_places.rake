require 'faker' 

namespace :db do
    
  desc "Create places"
  task :create_places => [:create_users , :environment] do
    Place.create({
          :name => "Woodland Park Zoo",
          :rating => 0,
          :category_id => 1,
          :description => "Woodland Park Zoo is a beautiful, 92-acre zoological garden featuring nearly 1,100 animals representing 300 species, many threatened or endangered in their native habitats. Woodland Park Zoo is noted for its award-winning, naturalistic exhibits, innovative educational programs and its leadership in more than three dozen worldwide field conservation projects in 30 countries. Woodland Park Zoo...Inspiring naturally!",
          :price => 2,
          :rating => 3.5,
          :age_group_ids => [1,2,3,4,5],
          :feature_ids => [12, 13, 14],
          :business_hours_attributes => {"1385937191865"=>{"day"=>"Weekdays", "open_time(5i)"=>"09:30:00", "close_time(5i)"=>"16:30:00", "closed"=>"0"}, 
              "1385937195873"=>{"day"=>"Weekends", "open_time(5i)"=>"09:30:00", "close_time(5i)"=>"16:30:00", "closed"=>"0"}},
          :street => "5500 Phinney Ave N",
          :phone_num => 2065482500,
          :website => "http://www.zoo.org",
          :user_id => 1,
          :state => "WA",
          :city => "Seattle",
          :zip_code => 98103,
          :latitude => 47.66913,
          :longitude => -122.353989,
    })
    
    Place.create({
          :name => "Twirl Cafe",
          :rating => 0,
          :category_id => 3,
          :description => "Twirl Cafe is a kid friendly cafe located on the top of Queen Anne in Seattle. Twirl features activities and classes for parents and kids, an enclosed play area, healthy food menu and coffee.",
          :price => 1,
          :rating => 1.5,
          :age_group_ids => [1,2,3,4],
          :feature_ids => [12, 17, 11],
          :business_hours_attributes => {"1385937191865"=>{"day"=>"Sunday", "open_time(5i)"=>"06:30:00", "close_time(5i)"=>"18:30:00", "closed"=>"1"}, 
              "1385937195873"=>{"day"=>"Monday", "open_time(5i)"=>"08:30:00", "close_time(5i)"=>"17:00:00", "closed"=>"0"}, 
              "1385937203024"=>{"day"=>"Tuesday", "open_time(5i)"=>"08:30:00", "close_time(5i)"=>"17:00:00", "closed"=>"0"}, 
              "1385937208648"=>{"day"=>"Wednesday", "open_time(5i)"=>"08:30:00", "close_time(5i)"=>"17:00:00", "closed"=>"0"}, 
              "1385937211310"=>{"day"=>"Thursday", "open_time(5i)"=>"08:30:00", "close_time(5i)"=>"20:00:00", "closed"=>"0"}, 
              "1385937217492"=>{"day"=>"Friday", "open_time(5i)"=>"08:30:00", "close_time(5i)"=>"20:00:00", "closed"=>"0"}, 
              "1385937220992"=>{"day"=>"Saturday", "open_time(5i)"=>"08:30:00", "close_time(5i)"=>"20:00:00", "closed"=>"0"}},
          :street => "2111 Queen Anne Ave N",
          :phone_num => 2062834552,
          :website => "http://www.twirlcafe.com",
          :user_id => 2,
          :state => "WA",
          :city => "Seattle",
          :zip_code => 98109,
          :latitude => 47.637576,
          :longitude => -122.357079
    })

    Place.create({
          :name => "The Barking Dog Alehouse",
          :rating => 0,
          :category_id => 5,
          :description => "Boisterous pub offering imaginative American fare & weekly rotating taps in eco-minded environs.",
          :price => 3,
          :rating => 1.5,
          :age_group_ids => [1,2,3,4,5],
          :feature_ids => [15, 16, 18],
          :business_hours_attributes => {"1385937191865"=>{"day"=>"Weekends", "open_time(5i)"=>"11:00:00", "close_time(5i)"=>"23:00:00", "closed"=>"0"}, 
              "1385937195873"=>{"day"=>"Weekdays", "open_time(5i)"=>"11:00:00", "close_time(5i)"=>"23:00:00", "closed"=>"0"}}, 
          :street => "705 NW 70th St",
          :phone_num => 2067822974,
          :website => "http://thebarkingdogalehouse.com/",
          :user_id => 2,
          :state => "WA",
          :city => "Seattle",
          :zip_code => 98117,
          :latitude => 47.679374,
          :longitude => -122.364825
    })
    puts "Created Places"
  end
  
end