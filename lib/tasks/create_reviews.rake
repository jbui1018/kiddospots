namespace :db do

  desc "Create Reviews"
  task :create_reviews => [:create_places, :environment] do
    min, max = 0.5, 5.0

    99.times do |n|
      Review.create({
              :user_id => 2,
              :review => Faker::Lorem.sentence(100),
              :rating => rand * (max-min) + min,
              :place_id => (n&1 == 0)? 2 : 1
      })
    end
    
    99.times do |n|
      Review.create({
              :user_id => 1,
              :review => Faker::Lorem.sentence(100),
              :rating => rand * (max-min) + min,
              :place_id => (n&1 == 0)? 2 : 1
      })
    end
    puts "Created Reviews"
  end
  
end