module SearchHelper
	def search_places(params, defaultDistanceKm, defaultDistanceMi) 
		@highlight_words = []
		latlong = []
		@search = Place.search(params[:q])

		@highlight_words = params[:name].split if params[:name] != nil

		# For the API, cookies will not present. Hence use lat & lon passed in the GET request
		if params[:q] != nil && params[:q][:lat] != nil && params[:q][:lon] != nil 
			latlong = [ params[:q][:lat], params[:q][:lon] ].map(&:to_f)
		else
		# For simple search when near is given 
			if cookies[:users_latlon] != nil # with cookie present
				latlong = JSON.parse(cookies[:users_latlon]) 
			else
				latlong = params[:near] if params[:near] != nil # with no cookie (API case)
			end
		end

		if params[:near] == "" && params[:name] == "" && params[:q] == nil
			@searchTemp = Place.solr_search do
				with(:location).in_radius(*latlong, defaultDistanceKm)
				any_of do
					with(:event_end_date).greater_than(Time.now)
					with(:event_end_date, nil)
				end
				order_by_geodist(:location, *latlong)
				paginate :page => params[:page], :per_page => 10
			end
			@places = @searchTemp.results
		else
			#This is an advanced search
			if params[:near] == nil && params[:name] == nil && params[:category_id] == nil
				if params[:expired_events] == nil
					@places = @search.result(distinct: true).not_expired
				else
					@places = @search.result(distinct: true)
				end
				#Advanced Search sorting based on distance
				if params[:q] != nil
					@highlight_words = params[:q]["name_cont"].split if params[:q]["name_cont"] != nil
				  	#If there is a city, then sort by closest to given city
					if params[:q][:city_cont] != nil
						@places = @places.near(params[:q][:city_cont], defaultDistanceMi)
					#If there is a zip code, then sort by closest to zip code
					elsif params[:q][:zip_code_eq] != nil
						@places = @places.near(params[:q][:zip_code_eq], defaultDistanceMi)
					#Else just sort by closest to the User's city
					else
						@places = @places.near(latlong, defaultDistanceMi)
					end
				else
					#No params, just sort by closest to user's city
					@places = @places.near(latlong, defaultDistanceMi)
				end

				@places = @places.paginate(page: params[:page], per_page: 10)
			else
				if params[:category_id] != nil
					@places = Place.not_expired.where(category_id: params[:category_id]).near(latlong, defaultDistanceMi)
         			@places = @places.paginate(page: params[:page], per_page: 10)
				else
					#No location, just a keyword, so we default to the User's City
					if params[:near] == ""
						@searchTemp = Place.solr_search do
							fulltext params[:name]
							any_of do
								with(:event_end_date).greater_than(Time.now)
								with(:event_end_date, nil)
							end
							with(:location).in_radius(*latlong, defaultDistanceKm);
							order_by_geodist(:location, *latlong)
							paginate :page => params[:page], :per_page => 10
						end
					else
						#Location is given so we use whatever keyword is given also
						nearCoord = *Geocoder.coordinates(params[:near])
						@searchTemp = Place.solr_search do
							fulltext params[:name]
							any_of do
								with(:event_end_date).greater_than(Time.now)
								with(:event_end_date, nil)
							end
							with(:location).in_radius(*nearCoord, defaultDistanceKm);
							order_by_geodist(:location, *nearCoord)
							paginate :page => params[:page], :per_page => 10
						end
					end	
					@places = @searchTemp.results
				end
			end
		end
		#ToDo: Add a sql call that will elimniate expired events
	end
end