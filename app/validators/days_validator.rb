class DaysValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
  	day_array = []
  	value.each do |bh|
  		if day_array.include? bh.day
  			record.errors[attribute] << (options[:message] || "day cannot be repeated")
  			break
  		else
  			day_array << bh.day
  		end
  	end
  end
end