class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
  	unless value =~ /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix
  		if url_valid?(value)
    		record.errors[attribute] << (options[:message] || "must be a valid URL of the form http://www.example.com")
    	end      
  end
end

  # a URL may be technically well-formed but may 
  # not actually be valid, so this checks for both.
  def url_valid?(url)
    url = URI.parse(url) rescue false
    url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)
  end 
end