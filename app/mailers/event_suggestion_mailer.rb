class EventSuggestionMailer < ActionMailer::Base
  default :to => "info@joeysearch.com"
  def new_event_suggestion(event_suggestion)
  	mail(:from => "info@joeysearch.com", :subject => "Event Suggestion: " + event_suggestion.name)
  end
end
