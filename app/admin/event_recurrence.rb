ActiveAdmin.register EventRecurrence do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  index do
    column :event_id
    column :recurrence_id
    column :weekly_day
    column :monthly_type
    column :monthly_date
    column :monthly_day
    column :monthly_week_num
    actions
  end

  permit_params :event_id, :recurrence_id, :weekly_day, :monthly_type, :monthly_date, :monthly_day, :monthly_week_num
  
end