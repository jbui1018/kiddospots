json.array!(@events) do |event|
  json.extract! event, :name, :description, :price, :street, :phone_num, :website, :user_id, :country, :state, :city, :zip_code, :slug, :latitude, :longitude, :start_date, :end_date, :recurrence_id
  json.url event_url(event, format: :json)
end