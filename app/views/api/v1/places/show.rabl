object @place

attributes :id, :name, :description, :rating, :price, :street, :city, :state, :zip_code, :website, :phone_num

node :latitude do |place|
	place.latitude.to_f
end

node :longitude do |place|
	place.longitude.to_f
end

node :category do |place|
	Category.where(:id => place.category_id).map(&:name).first
end

node :features do |place|
	place.features.map(&:name)
end

node :age_groups do |place|
	place.age_groups.map(&:group_name)
end

node :business_hours do |place|
	place.business_hours.to_a.map { |p| { :id => p.id, :day => p.day, :open_time => p.open_time.to_time.strftime("%I:%M %p"), 
									:close_time => p.close_time.to_time.strftime("%I:%M %p"), :closed => p.closed } }
end

node :num_reviews do |place|
	place.reviews.count
end

node :thumbnails do |place|
	if place.photos.first
		place.photos.to_a.map(&:image).to_a.map(&:thumb).to_a.map(&:url)
	else
		[]
	end
end