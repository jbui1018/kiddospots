collection @places

attributes :id, :name, :rating, :price, :street, :city, :state, :zip_code, :website, :phone_num

node :latitude do |place|
	place.latitude.to_f
end

node :longitude do |place|
	place.longitude.to_f
end

node :category do |place|
	Category.where(:id => place.category_id).map(&:name).first
end


node :thumbnail do |place|
	if place.photos.first
		place.photos.first.image_url(:thumb).to_s
	else
		""
	end
end

node :num_reviews do |place|
	place.reviews.count
end

node(:total_pages) {|m| @places.total_pages }