object @review

node :success do 
	true
end

node :review_id do |review|
	review.id
end

node(:review_votes) do |review|
	review.votes
end