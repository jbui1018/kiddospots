collection @reviews

attributes :id, :review, :rating

node(:review_votes) do |review|
	review.votes
end

node(:created_by) do |review|  
	{:id => review.user.id, :name => name_format(review.user.name)}
end

node(:updated_at){|review| review.updated_at.strftime("%m/%d/%Y")}

node (:photos) do |review|
	review.photos.to_a.map { |r| { :url => r.image.url, :thumb => r.image.thumb.url} }
end

node(:total_pages) {|m| @reviews.total_pages }