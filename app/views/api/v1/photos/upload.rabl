object @photo

node :success do 
	true
end

node :photo_id do |photo|
	photo.id
end

node :photo do |photo|
	photo.image
end
