collection @photos

attributes :id, :description

node(:uploaded_by) do |photo|  
	{:id => photo.user_id, :name => name_format(User.find(photo.user_id).name)}
end

child :image => :image do 
    attributes :url
    node :thumb do |image|
    	image.thumb.url
    end
end