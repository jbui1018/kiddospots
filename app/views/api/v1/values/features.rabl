collection @categories_features

attributes :category_id, :feature_id

node :feature_name do |f|
	f.feature.name
end