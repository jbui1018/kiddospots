json.array!(@photos) do |photo|
  json.extract! photo, :place_id, :description, :image, :user_id
  json.url photo_url(photo, format: :json)
end