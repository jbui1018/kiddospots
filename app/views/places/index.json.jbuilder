json.array!(@places) do |place|
  json.extract! place, :name, :rating, :category_id, :description, :price, :address, :phone_num, :website
  json.url place_url(place, format: :json)
end