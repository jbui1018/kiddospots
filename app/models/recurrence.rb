class Recurrence < ActiveRecord::Base
	has_many :places
	has_many :events, through: :event_recurrences
end
