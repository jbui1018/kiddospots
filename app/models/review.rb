class Review < ActiveRecord::Base
	belongs_to :place
	belongs_to :user
	has_many :reviews_age_groups, dependent: :destroy
	has_many :age_groups, through: :reviews_age_groups
	has_many :photos, dependent: :destroy
	has_many :review_votes, dependent: :destroy

	validates :rating, :inclusion => { :in => 0..5, :message => " should be between 0 to 5" }
	validate :user_has_not_reviewed_place?, :on => :create
	default_scope -> { order('created_at DESC') }

	def self.by_votes
	 	select('reviews.*, coalesce(value, 0) as votes').
	 	joins('left join review_votes on review_id=reviews.id').
	 	order('votes desc')
	end

	def votes
	 	read_attribute(:votes) || review_votes.sum(:value)
	end

	private
		def user_has_not_reviewed_place?
			@similar = Review.where(user_id: user_id)
			puts @similar
			if @similar.count == 0
				return
			else
				@similar.each do |review|
					if review.place_id == place_id
						errors.add(:user_id, 'has already reviewed this spot, please edit your previous review')
					end
				end
			end
		end
end
