class AgeGroup < ActiveRecord::Base
	has_many :places_age_group
	has_many :places, through: :places_age_group
end
