class Category < ActiveRecord::Base
	has_many :places
	has_many :categories_features, dependent: :destroy
	has_many :features, through: :categories_features
end