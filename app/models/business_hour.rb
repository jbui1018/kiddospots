class BusinessHour < ActiveRecord::Base
	belongs_to :place
	validates :day, :uniqueness => {:scope => :place_id, message: "cannot be repeated"}
end
