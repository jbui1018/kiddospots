class Event < ActiveRecord::Base
	belongs_to :user
	validates :name, presence: true
	validates :street, presence: true 
	validates :city, presence: true 
	validates :state, presence: true 
	validates_numericality_of :zip_code
	validates :website, url: true
	validates :user_id, presence: true
	#validates :price, :format => { :with => /\A\d+(?:\.\d{0,2})?\z/ }, :numericality => {:greater_than_or_equal_to => 0.00}, allow_blank: true
	# validates_presence_of :recurrence_id
	validates_presence_of :start_date
	#validate :end_date_is_possible?
	#validate :event_not_already_present?, :on => :create
	has_many :event_recurrences, dependent: :destroy
	accepts_nested_attributes_for :event_recurrences, allow_destroy: true

	geocoded_by :address
	before_validation :geocode, :if => :address_changed?

	mount_uploader :image, ImageUploader

	def happens_once?
		# recurrence_id == 1
	end

	extend FriendlyId
	friendly_id :slug_candidates, use: [:slugged, :finders]

	private
		def end_date_is_possible?
			# return if end_date.blank?
			# if end_date < Date.today
			# 	errors.add(:event_end_date, 'is already past todays date')
			# end
			# return if [start_date.blank?, end_date.blank?].any?
		 #    if end_date < start_date
		 #    	errors.add(:end_date, 'is earlier than start date!')
			# end
		end

		# Helpers for address Geocoding
		def address
		  [street, city, state, zip_code].compact.join(', ')
		end

		def address_changed?
			:street_changed? || :city_changed? || :state_changed? || :zipcode_changed? 
		end

		def event_not_already_present?
			# @similar = Event.where(name: name)
			# if @similar.count == 0
			# 	return
			# else
			# 	@similar.each do |place|
			# 		if Rails.env.production?
			# 			if event.longitude == longitude.round(6) && event.latitude == latitude.round(6)
			# 				errors.add(:name, 'already exists at this location!')
			# 			end
			# 		else
			# 			if event.longitude == longitude && event.latitude == latitude
			# 				errors.add(:name, 'already exists at this location!')
			# 			end
			# 		end
			# 	end
			# end
		end

		# Try building a slug based on the following fields in
		# increasing order of specificity.
		def slug_candidates
		  [
			:name,
			[:name, :city],
			[:name, :zip_code, :city]
		  ]
		end
end
