class EventSuggestion < ActiveRecord::Base
	belongs_to :user
	validates :name, presence: true
	validates :street, presence: true 
	validates :city, presence: true 
	validates :state, presence: true 
	validates_numericality_of :zip_code
	validates :website, url: true
	validates :user_id, presence: true
end
