class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  before_save :ensure_authentication_token
  # before_save { self.email = email.downcase }
  validates :name, presence: true, length: { maximum: 50 }
  # VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }

  has_many :places
  has_many :events
  has_many :reviews, dependent: :destroy
  has_many :review_votes
  has_many :place_votes

  mount_uploader :image, ImageUploader

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
      user = User.where(:provider => auth.provider, :uid => auth.uid).first
      if user
        return user
      else
        registered_user = User.where(:email => auth.info.email).first
        if registered_user
          return registered_user
        else
          user = User.create(name:auth.extra.raw_info.name,
                              provider:auth.provider,
                              uid:auth.uid,
                              email:auth.info.email,
                              password:Devise.friendly_token[0,20],
                            )
        end
      end
    end  

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]


  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end



  def total_votes
    ReviewVote.joins(:review).where(reviews: {user_id: self.id}).sum('value')
  end

  def voted_up_yet?(review)
    ReviewVote.where(user_id: self.id, review_id: review.id).where(value: 1).present?
  end

  def total_place_votes
    PlaceVote.joins(:place).where(places: {user_id: self.id}).sum('value')
  end

  def voted_place_up_yet?(place)
    PlaceVote.where(user_id: self.id, place_id: place.id).where(value: 1).present?
  end
 
  private
  
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

end
