class Photo < ActiveRecord::Base
	validates :image, :presence => true
	belongs_to :place
	belongs_to :review

	mount_uploader :image, ImageUploader
	# validates_presence_of :image

	def default_description
	  self.description ||= File.basename(image.filename, '.*').titleize if image
	end

	def next
		place.photos.where("id > ?", id).order("id ASC").first
	end

	def prev
		place.photos.where("id < ?", id).order("id DESC").first
	end
	
end