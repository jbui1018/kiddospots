class PlaceVote < ActiveRecord::Base
	belongs_to :place
	belongs_to :user

	validates_uniqueness_of :place_id, scope: :user_id
	validates_inclusion_of :value, in: [1, 0]
end