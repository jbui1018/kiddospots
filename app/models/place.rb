class Place < ActiveRecord::Base
	belongs_to :category
	has_many :reviews, dependent: :destroy
	belongs_to :user
	has_many :places_age_groups, dependent: :destroy
	has_many :age_groups, through: :places_age_groups
	has_many :places_features, dependent: :destroy
	has_many :features, through: :places_features
	has_many :business_hours, dependent: :destroy
	accepts_nested_attributes_for :business_hours, allow_destroy: true
	has_many :photos, dependent: :destroy
	has_many :place_votes, dependent: :destroy

	validates :name, presence: true 
	validates :category_id, presence: true 
	validates :street, presence: true 
	validates :city, presence: true 
	validates :state, presence: true 
	validates_numericality_of :zip_code
	validates :website, url: true
	validates :user_id, presence: true
	validates :business_hours, days: true
	validates :event_price, :format => { :with => /\A\d+(?:\.\d{0,2})?\z/ }, :numericality => {:greater_than_or_equal_to => 0.00}, allow_blank: true
	validates_presence_of :event_start_date, :unless => :not_event_type?
	validates_presence_of :recurrence_id, :unless => :not_event_type?
	validates_presence_of :event_end_date, :if => :happens_once?, :message => "must be present if the event only occurs once"
	validate :end_date_is_possible?
	validate :place_not_already_present?, :on => :create
	before_save :add_time_to_hours

	# Custom error message in form validation
	validate do |age_group|
		age_group.errors[:base] << "An Age Group must be selected" if age_group.age_group_ids.blank?
	end

	geocoded_by :address
	before_validation :geocode, :if => :address_changed?

	# Query to eliminate expired events
	scope :not_expired, -> {
		where("event_end_date >= ? OR event_end_date IS NULL", Date.today)
	}

	# Query to check if the start date is before a certain dat
	scope :starts_before, ->(day) { 
		where("event_start_date <= ? OR event_start_date IS NULL", day)
	}

	searchable do
		text :name, :boost => 5
		text :description
		text :category do
			category.name if category
		end
		text :features do |place|
			place.features.map{|feature| feature.name}
		end
		text :reviews do
			reviews.map(&:review)
		end
		# location :location
		latlon(:location) { Sunspot::Util::Coordinates.new(latitude, longitude)}
		time :event_end_date
	end

	# Helper for custom validation for website. Adds http:// if not present
	def website= url_str
	  unless url_str.blank?
		unless url_str.split(':')[0] == 'http' || url_str.split(':')[0] == 'https'
			url_str = "http://" + url_str
		end
	  end
	  write_attribute :website, url_str
	end

	# Strip non-digit characters from phone number
	def phone_num= phone_str
		phone_str.to_s.gsub!(/\D/, '')
		write_attribute :phone_num, phone_str
	end

	# Performs search on name & city
	def self.search_simple near, name
		return scoped unless near.present? || name.present?
		if name != "" && near != ""
			return where(['name LIKE ?', "%#{name}%"]) & Place.near("%#{near}%",40) 
		elsif near == ""
			return where(['name LIKE ?', "%#{name}%"])  
		elsif name == ""
			Place.near("%#{near}%",40) 
		end
	end	

	extend FriendlyId
	friendly_id :slug_candidates, use: [:slugged, :finders]

	def self.by_votes
	 	select('places.*, coalesce(value, 0) as votes').
	 	joins('left join place_votes on place_id=places.id').
	 	order('votes desc')
	end

	def votes
	 	read_attribute(:votes) || place_votes.sum(:value)
	end

	private
		def end_date_is_possible?
			return if event_end_date.blank?
			if event_end_date < Date.today
				errors.add(:event_end_date, 'is already past todays date')
			end
			return if [event_start_date.blank?, event_end_date.blank?].any?
		    if event_end_date < event_start_date
		    	errors.add(:event_end_date, 'is earlier than start date!')
			end
		end

		def place_not_already_present?
			@similar = Place.where(name: name)
			if @similar.count == 0
				return
			else
				@similar.each do |place|
					if Rails.env.production?
						if place.longitude == longitude.round(6) && place.latitude == latitude.round(6)
							errors.add(:name, 'already exists at this location!')
						end
					else
						if place.longitude == longitude && place.latitude == latitude
							errors.add(:name, 'already exists at this location!')
						end
					end
				end
			end
		end

		def not_event_type?
			category_id != 7
		end

		def happens_once?
			recurrence_id == 1
		end

		# Helpers for address Geocoding
		def address
		  [street, city, state, zip_code].compact.join(', ')
		end

		def address_changed?
			:street_changed? || :city_changed? || :state_changed? || :zipcode_changed? 
		end

		# Try building a slug based on the following fields in
		# increasing order of specificity.
		def slug_candidates
		  [
			:name,
			[:name, :city],
			[:name, :zip_code, :city]
		  ]
		end

		@@timezone = Timezone::Zone.new :zone => 'America/Los_Angeles'

		def add_time_to_hours

			is_dst_on = @@timezone.dst?(Time.now)
			if is_dst_on
				@time_to_add = 1.hour
			else
				@time_to_add = 0
			end

   			unless self.business_hours.blank?
   				self.business_hours.each do |k|
					k.open_time = k.open_time + @time_to_add
   					k.close_time = k.close_time + @time_to_add
   				end
   			end

		end
end