class Feature < ActiveRecord::Base
	# belongs_to :category
	has_many :places_features
	has_many :categories_features
	has_many :places, through: :places_features
	has_many :categories, through: :categories_features
end