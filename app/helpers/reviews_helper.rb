module ReviewsHelper

	def averageRating(place_id)
		Review.average(:rating, :conditions => ['place_id =?', place_id])
	end	

	def user_not_already_reviewed?(current_user_id, place_id)
		Review.where(:user_id => current_user_id).where(:place_id => place_id).count == 0
	end

	# Returns an array of age_group_ids. Used in create/edit form to populate checkbox values
	def getReviewAgeGroupIDs(review_id)
	  age_group_id_array = []
	  ReviewsAgeGroup.where(review_id: review_id).each do | reviews_age_group |
	    age_group_id_array << reviews_age_group.age_group_id
	  end
	  age_group_id_array
	end
end
