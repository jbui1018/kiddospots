module EventsHelper

  	def eventDaysOfTheWeek
		["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"] 
  	end

  	def eventDaysOfTheMonth
  		[1.ordinalize,2.ordinalize,3.ordinalize,4.ordinalize,5.ordinalize,6.ordinalize,7.ordinalize,8.ordinalize,9.ordinalize,10.ordinalize,
  			11.ordinalize,12.ordinalize,13.ordinalize,14.ordinalize,15.ordinalize,16.ordinalize,17.ordinalize,18.ordinalize,19.ordinalize,20.ordinalize,
  			21.ordinalize,22.ordinalize,23.ordinalize,24.ordinalize,25.ordinalize,26.ordinalize,27.ordinalize,28.ordinalize,29.ordinalize,30.ordinalize,31.ordinalize] 
  	end

  	def eventMonthRecurrenceType
  		["Same date every month (i.e. 15th)", "Specific day of the month (i.e. 3rd Tuesday)"]
  	end

  	def numWeeksInAMonth
  		[1.ordinalize, 2.ordinalize, 3.ordinalize, 4.ordinalize, 5.ordinalize]
  	end

    def eventsThatDay(date)
      date_num = date.strftime('%d')
      day = date.strftime('%A')
      week_num = date_num.to_i/7
      unless date_num.to_i.modulo(7).zero?
        week_num = week_num + 1
      end
      @eventsOnce = Event.where("start_date <= ?", date).where("end_date >= ? OR end_date IS NULL", date).includes(:event_recurrences).
        where("event_recurrences.recurrence_id = 1").references(:event_recurrences)
      @events_daily = Event.where("start_date <= ?", date).where("end_date >= ? OR end_date IS NULL", date).includes(:event_recurrences).
        where("event_recurrences.recurrence_id = 2").references(:event_recurrences)
      @events_weekly = Event.where("start_date <= ?", date).where("end_date >= ? OR end_date IS NULL",date).includes(:event_recurrences).
        where("event_recurrences.recurrence_id = 3").where("lower(weekly_day) = ?", day.downcase).references(:event_recurrences)
      @events_monthly_date = Event.where("start_date <= ?", date).where("end_date >= ? OR end_date IS NULL", date).includes(:event_recurrences).
        where("event_recurrences.recurrence_id = 4").where("monthly_type = 0").where("monthly_date = ?", date_num).references(:event_recurrences)
      @events_monthly_day = Event.where("start_date <= ?", date).where("end_date >= ? OR end_date IS NULL", date).includes(:event_recurrences).
        where("event_recurrences.recurrence_id = 4").where("monthly_type = 1").where("lower(monthly_day) = ?", day.downcase).where("monthly_week_num = ?", week_num).references(:event_recurrences)
      @events = (@eventsOnce + @events_daily + @events_weekly + @events_monthly_date + @events_monthly_day).uniq.sort_by!{ |e| e.name.downcase }
      @events = @events.paginate(page: params[:page], per_page: 10)
    end
end
