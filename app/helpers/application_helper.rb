module ApplicationHelper
	
	# Returns the full title on a per-page basis.
	def full_title(page_title)
		base_title = "Joeysearch"
		if page_title.empty?
			base_title
		else
			"#{base_title} | #{page_title}"
		end
	end

	def name_format(username)
		names = username.split
		firstname = names.first
		if names.size <= 1
			username = firstname
		else
			initial = names.last[0,1]
			username = firstname + " " + initial
		end
	end

end
