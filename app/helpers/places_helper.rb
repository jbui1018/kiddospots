module PlacesHelper
	def us_states
	    [
	      ['Alabama', 'AL'],
	      ['Alaska', 'AK'],
	      ['Arizona', 'AZ'],
	      ['Arkansas', 'AR'],
	      ['California', 'CA'],
	      ['Colorado', 'CO'],
	      ['Connecticut', 'CT'],
	      ['Delaware', 'DE'],
	      ['District of Columbia', 'DC'],
	      ['Florida', 'FL'],
	      ['Georgia', 'GA'],
	      ['Hawaii', 'HI'],
	      ['Idaho', 'ID'],
	      ['Illinois', 'IL'],
	      ['Indiana', 'IN'],
	      ['Iowa', 'IA'],
	      ['Kansas', 'KS'],
	      ['Kentucky', 'KY'],
	      ['Louisiana', 'LA'],
	      ['Maine', 'ME'],
	      ['Maryland', 'MD'],
	      ['Massachusetts', 'MA'],
	      ['Michigan', 'MI'],
	      ['Minnesota', 'MN'],
	      ['Mississippi', 'MS'],
	      ['Missouri', 'MO'],
	      ['Montana', 'MT'],
	      ['Nebraska', 'NE'],
	      ['Nevada', 'NV'],
	      ['New Hampshire', 'NH'],
	      ['New Jersey', 'NJ'],
	      ['New Mexico', 'NM'],
	      ['New York', 'NY'],
	      ['North Carolina', 'NC'],
	      ['North Dakota', 'ND'],
	      ['Ohio', 'OH'],
	      ['Oklahoma', 'OK'],
	      ['Oregon', 'OR'],
	      ['Pennsylvania', 'PA'],
	      ['Puerto Rico', 'PR'],
	      ['Rhode Island', 'RI'],
	      ['South Carolina', 'SC'],
	      ['South Dakota', 'SD'],
	      ['Tennessee', 'TN'],
	      ['Texas', 'TX'],
	      ['Utah', 'UT'],
	      ['Vermont', 'VT'],
	      ['Virginia', 'VA'],
	      ['Washington', 'WA'],
	      ['West Virginia', 'WV'],
	      ['Wisconsin', 'WI'],
	      ['Wyoming', 'WY']
	    ]
  	end

  	def daysOfTheWeek
  		# [["Sunday", 0], ["Monday", 1], ["Tuesday", 2], ["Wednesday", 3], ["Thursday", 4], ["Friday", 5], ["Saturday", 6]]
  		["Weekdays", "Weekends", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"] 
  	end

  	def intToDay(n)
  		Date::DAYNAMES[n]
  	end

  	def priceDollars
  		[['Free', 0],['$', 1],['$$', 2],['$$$', 3],['$$$$', 4]]
  	end

  	def intToPrice(n)
  		prices = {0=>"Free", 1=>"$", 2=>"$$", 3=>"$$$", 4=>"$$$$"}
  		prices[n]
  	end

  	# Returns an array of age_group_ids. Used in create/edit form to populate checkbox values
	def getAgeGroupIDs(place_id)
	  age_group_id_array = []
	  PlacesAgeGroup.where(place_id: place_id).each do | places_age_group |
	    age_group_id_array << places_age_group.age_group_id
	  end
	  age_group_id_array
	end

	# Returns an array of age_group_ids. Used in create/edit form to populate checkbox values
	def getFeatureIDs(place_id)
	  feature_id_array = []
	  PlacesFeature.where(place_id: place_id).each do | places_feature |
	    feature_id_array << places_feature.feature_id
	  end
	  feature_id_array
	end

end