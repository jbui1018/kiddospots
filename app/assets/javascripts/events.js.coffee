# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
	# For Nested Fields - Used in adding hours in create places page
	$ ->
		$(document).ready (e) ->
		$('body form').nestedFields();

	#Scripts to make the datepicker appear for start and end date.
	$( ".event_date" ).datepicker({
		defaultDate: Date.today,
		dateFormat: "yy-mm-dd",
	})
	$( "#event_recurrence_end" ).datepicker({
		defaultDate: Date.today,
		dateFormat: "yy-mm-dd",
	})
