jQuery ->
  # For Categories on the create place page
  $('#place_feature_ids').parent().hide()
  features = $('#place_feature_ids').html()
  category = $('#place_category_id :selected').text()
  if category.length > 0 
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(features).filter("optgroup[label='#{escaped_category}']").html()
    if options
        $('#place_feature_ids').html(options)
        $('#place_feature_ids').parent().show()
  $('#place_category_id').change ->
    category = $('#place_category_id :selected').text()
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(features).filter("optgroup[label='#{escaped_category}']").html()
    if options
      $('#place_feature_ids').html(options)
      $('#place_feature_ids').parent().show()
    else
      $('#place_feature_ids').empty()
      $('#place_feature_ids').parent().hide()

jQuery ->  
  # For Categories on the advanced search page
  $('#q_features_id_eq_any').hide()
  features = $('#q_features_id_eq_any').html()
  category = $('#place_category_id, #q_category_id_eq :selected').text()
  if category.length > 0 
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(features).filter("optgroup[label='#{escaped_category}']").html()
    if options
        $('#q_features_id_eq_any').html(options)
        $('#q_features_id_eq_any').show()
  $('#place_category_id, #q_category_id_eq').change ->
    category = $('#place_category_id, #q_category_id_eq :selected').text()
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(features).filter("optgroup[label='#{escaped_category}']").html()
    if options
      $('#q_features_id_eq_any').html(options)
      $('#q_features_id_eq_any').show()
    else
      $('#q_features_id_eq_any').empty()
      $('#q_features_id_eq_any').hide()