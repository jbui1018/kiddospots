jQuery(function() {
  var coords_obj, handler;

  coords_obj = $('#map').data('coords');

  if (coords_obj && coords_obj.length <= 1) {
    handler = Gmaps.build('Google', {
      markers: {
        maxRandomDistance: null
      }
    });
  } else {
    handler = Gmaps.build('Google', {
      markers: {
        maxRandomDistance: 10
      }
    });
  }

  handler.buildMap({
    provider: {},
    internal: {
      id: "map"
    }
  }, function() {
    var i, infoWindow, m, marker, markers, _i, _len;
    markers = handler.addMarkers(coords_obj);
    handler.bounds.extendWith(markers);
    handler.fitMapToBounds();
    if (markers.length === 1) {
      handler.getMap().setZoom(15);
    }
    infoWindow = new google.maps.InfoWindow({
      content: coords_obj[0].infowindow
    });
    i = 1;
    for (_i = 0, _len = markers.length; _i < _len; _i++) {
      m = markers[_i];
      marker = m.serviceObject;
      marker.icon = "/assets/largeTDRedIcons/marker" + i + ".png";
      i += 1;
    }
    return google.maps.event.addListener(infoWindow, "domready", function() {
      return $("#review_value").raty({
        readOnly: true,
        halfShow: true,
        noRatedMsg: "No ratings yet",
        score: function() {
          return $(this).attr("data-rating");
        }
      });
    });
  });
});