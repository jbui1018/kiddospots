//= require jquery.raty.min

//Places: Show Star Rating
jQuery(function() {
	$(function() {
	  return $("div[id^=\"review_value\"]").each(function() {
	    return $(this).raty({
	      readOnly: true,
	      halfShow: true,
	      noRatedMsg: "No ratings yet",
	      score: function() {
	        return $(this).attr("data-rating");
	      }
	    });
	  });
	});
	return $("#star").raty({
	  score: $("#val").data("rating"),
	  half: true,
	  click: function(score, evt) {
	    return $("#rating").val(score);
	  }
	});
});