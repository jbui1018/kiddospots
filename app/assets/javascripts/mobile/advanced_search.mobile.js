jQuery(function() {

  var category, escaped_category, features, options;
  features_count = gon.features_count;
  var elementID = "";

  for ( var i = 1; i <= features_count; i++ ) {
    elementID = "#q_features_id_eq_any_" + i;
    $(elementID).parent().hide();
  }

  category = $('#q_category_id_eq :selected').val();
  if (category > 0) {
    options = setOptions(category);

    if (options.length > 0) {
      for ( var j = 0; j <= options.length-1; j++ ) {      
        elementID = "#q_features_id_eq_any_" + options[j].feature_id;
        $(elementID).parent().show();
      }
    }
  }else {
      $('#features_listview').hide();
  }
  return $('#q_category_id_eq').change(function() {
    category = $('#q_category_id_eq :selected').val();
    if (category)
      options = setOptions(category);
    else 
      options = $();

    for ( var i = 1; i <= features_count; i++ ) {
      elementID = "#q_features_id_eq_any_" + i;
      $(elementID).parent().hide();
      $(elementID).attr("checked",false).checkboxradio('refresh');
    }

    if (options.length > 0) {
      for ( var j = 0; j <= options.length-1; j++ ) {      
        elementID = "#q_features_id_eq_any_" + options[j].feature_id;
        $(elementID).parent().show();
      }
      $('#features_listview').show();
      return
    }else {
      $('#features_listview').hide();
    }
  });

  function  setOptions(category) {
    var options;

    if (category == 1) //Parks/Playgrounds
      options = gon.park_features;
    else if (category == 2) //Museums/Aquariums/Zoos
      options = gon.museum_features;
    else if (category == 3) //Restaurants/Cafes
      options = gon.restaurant_features;
    else if (category == 4) //Pools/Water Parks
      options = gon.pool_features;
    else if (category == 5) //Movie Theaters
      options = gon.movie_features;
    else if (category == 6) //Classes
      options = gon.class_features;
    else if (category == 7) //Events
      options = gon.event_features;
    else if (category == 8) //Retail Stores
      options = gon.retail_features;
    else if (category == 9) //Schools/Day Care
      options = gon.school_features;
    else if (category == 10) //Dentists/Physicians/Health
      options = gon.dentist_features;
    else if (category == 11) //Amusement Parks
      options = gon.amusement_features;
    else if (category == 12) //Hotel/Accommodations
      options = gon.hotel_features;
    else if (category == 13) //Attractions
      options = gon.attraction_features;
    else if (category == 14) //Other
      options = gon.other_features;
    else if (category == 15) //Indoor Activity Center
      options = gon.indoor_features;
    else if (category == 16) //Salon
      options = gon.salon_features;
    else if (category == 17) //Library
      options = gon.library_features;

    return options;
  }

});