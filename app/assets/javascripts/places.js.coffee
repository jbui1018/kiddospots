# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  # Hide event fields based on if event is the selected category
  $('#place_event_start_date').parent().hide()
  $('#place_event_end_date').parent().hide()
  $('#place_recurrence_id').parent().hide()
  $('#place_event_price').parent().hide()
  category = $('#place_category_id :selected').text()
  if category is 'Events'
    $('#place_event_start_date').parent().show()
    $('#place_event_end_date').parent().show()
    $('#place_recurrence_id').parent().show()
    $('#place_event_price').parent().show()

  #Scripts to make the datepicker appear for start and end date.
  $( "#place_event_start_date" ).datepicker({
      defaultDate: Date.today,
      dateFormat: "yy-mm-dd",
    })
  $( "#place_event_end_date" ).datepicker({
      defaultDate: Date.today,
      dateFormat: "yy-mm-dd",
    })

  $('#place_category_id').change ->
    category = $('#place_category_id :selected').text()
    if category is 'Events'
      $('#place_event_start_date').parent().show()
      $('#place_event_end_date').parent().show()
      $('#place_recurrence_id').parent().show()
      $('#place_event_price').parent().show()
    else
      $('#place_event_start_date').parent().hide()
      $('#place_event_end_date').parent().hide()
      $('#place_recurrence_id').parent().hide()
      $('#place_event_price').parent().hide()

# To show star rating ###########
  $ ->
    $("div[id^=\"review_value\"]").each ->
      $(this).raty
        readOnly: true
        halfShow: true
        noRatedMsg: "No ratings yet"
        score: ->
          $(this).attr "data-rating"

  $("#star").raty
      score: $('#val').data('rating')
      half: true 
      hints: ["Do not bring your kids!", '', '', '', 'Perfect place for kids!']
      click: (score, evt) ->
        $("#rating").val score
#################################

  # For Nested Fields - Used in adding hours in create places page
  $ ->
    $(document).ready (e) ->
      $('body form').nestedFields();

  # Show/Hidle search div
  $(".searchbutton").click ->
    $searchbutton = $(this)
    $content = $searchbutton.next()
    $content.slideToggle 500, ->
      $searchbutton.text ->
        (if $content.is(":visible") then "Hide Search" else "Show Search")

  # Show/Hidle change location form
  $("#locationform").hide()
  $(".changelocationbutton").click ->
    $changelocationbutton = $(this)
    $content = $changelocationbutton.next()
    $content.slideToggle 500, ->
      $changelocationbutton.text ->
        (if $content.is(":visible") then "Hide" else "Change Location")

  # Handling maps initial zoom
  coords_obj = $('#map').data('coords')

  # If there is only 1 marker, we don't need a random distance, if there is more, we'll add a slight one just in case there are a bunch of markers bunched together
  if coords_obj and coords_obj.length <= 1
    handler = Gmaps.build 'Google', { markers: { maxRandomDistance: null } }
  else
    handler = Gmaps.build 'Google', { markers: { maxRandomDistance: 10 } }
  handler.buildMap
    provider: {}
    internal:
      id: "map"
  , ->
    markers = handler.addMarkers(coords_obj)
    handler.bounds.extendWith markers
    handler.fitMapToBounds()

    # If there is only 1 marker, zoom out to a zoom level that makes sense.
    if markers.length == 1
      handler.getMap().setZoom(15)

    infoWindow = new google.maps.InfoWindow(content: coords_obj[0].infowindow) # Create an InfoWindow object from the coords data passed from the controller
    i = 1
    for m in markers
      marker = m.serviceObject
      # infoWindow.open(map, marker) # Shows InfoWindow on page load. Without this, the star rating doesn't seem to work
      marker.icon = "/assets/largeTDRedIcons/marker"+i+".png"
      i+=1

    # google.maps.event.addListener marker, "mouseover", ->
    google.maps.event.addListener infoWindow, "domready", ->
      $("#review_value").raty
        readOnly: true
        halfShow: true
        noRatedMsg: "No ratings yet"
        score: ->
          $(this).attr "data-rating"