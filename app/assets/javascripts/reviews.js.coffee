# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->  
  $("#star").raty 
  	half: true 
  	click: (score, evt) ->
    	$("#rating").val score

# To display stars for the retrieved rating
  $ ->
    $("div[id^=\"review_value\"]").each ->
      $(this).raty
        readOnly: true
        score: ->
          $(this).attr "data-rating"


      $(this).next().text $(this).attr("data-score")