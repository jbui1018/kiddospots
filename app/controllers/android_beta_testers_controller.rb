#Android Beta
class AndroidBetaTestersController < ApplicationController
  before_action :set_android_beta_tester, only: [:show, :edit, :update, :destroy]

  # GET /android_beta_testers
  # GET /android_beta_testers.json
  def index
    @android_beta_testers = AndroidBetaTester.all
  end

  # GET /android_beta_testers/1
  # GET /android_beta_testers/1.json
  # def show
  # end

  # GET /android_beta_testers/new
  def new
    @android_beta_tester = AndroidBetaTester.new
  end

  # GET /android_beta_testers/1/edit
  # def edit
  # end

  # POST /android_beta_testers
  # POST /android_beta_testers.json
  def create
    @android_beta_tester = AndroidBetaTester.new(android_beta_tester_params)

    respond_to do |format|
      if @android_beta_tester.save
        format.html { redirect_to root_url, notice: 'You have successfully signed up for Android Beta Program. A member of the Joeysearch team will contact 
          shortly you with instructions on how to install the Android Beta App.' }
        format.mobile { redirect_to root_url, notice: 'You have successfully signed up for Android Beta Program. A member of the Joeysearch team will contact 
          shortly you with instructions on how to install the Android Beta App.' }
      else
        format.html { render action: 'new' }
        format.mobile { render action: 'new' }
      end
    end
  end

  # PATCH/PUT /android_beta_testers/1
  # PATCH/PUT /android_beta_testers/1.json
  # def update
  #   respond_to do |format|
  #     if @android_beta_tester.update(android_beta_tester_params)
  #       format.html { redirect_to @android_beta_tester, notice: 'Android beta tester was successfully updated.' }
  #       format.json { head :no_content }
  #     else
  #       format.html { render action: 'edit' }
  #       format.json { render json: @android_beta_tester.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /android_beta_testers/1
  # DELETE /android_beta_testers/1.json
  # def destroy
  #   @android_beta_tester.destroy
  #   respond_to do |format|
  #     format.html { redirect_to android_beta_testers_url }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_android_beta_tester
      @android_beta_tester = AndroidBetaTester.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def android_beta_tester_params
      params.require(:android_beta_tester).permit(:name, :email)
    end
end
