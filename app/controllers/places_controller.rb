require 'search_helper'

class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:update, :new, :edit]

  include SearchHelper
  
  respond_to :html, :json
  # GET /places
  # GET /places.json

  def index
    @map_coords_hash =[]
    if mobile_device?
      set_features
    end

    #default search distance
    defaultDistanceKm = 100
    defaultDistanceMi = 63

    @places = search_places(params, defaultDistanceKm, defaultDistanceMi)

    # For GMaps Infowindow
    @places.each do |place|
      category = Category.find(place.category_id).name
      if mobile_device?
        @map_coords_hash << {:lat => place.latitude, :lng => place.longitude, 
        :infowindow => render_to_string(:partial => "welcome/infowindow", :locals => { :place_obj => place }) }
      else
        @map_coords_hash << {:lat => place.latitude, :lng => place.longitude, 
        :infowindow => render_to_string(:partial => "welcome/infowindow", :formats => [:html, :json], :locals => { :place_obj => place }) }
      end
    end

    respond_with @places

  end

  # GET /places/1
  # GET /places/1.json
  def show
    @place = Place.find(params[:id])
    @review = @place.reviews.build
    @reviews = @place.reviews.paginate(page: params[:page],
                                      per_page: 10)

    # For GMaps Infowindow
    @map_coords_hash = Gmaps4rails.build_markers(@place) do |place, marker|
      marker.lat place.latitude
      marker.lng place.longitude
      unless mobile_device?
        marker.infowindow render_to_string(:partial => "places/infowindow", :formats => [:html, :json], :locals => { :place_obj => place })
      end
    end

    respond_to do |format|
      format.html
      # format.json {render json: @place.as_json(include: { age_groups: { only: [:id, :group_name] }, reviews: {} }) }
    end

  end

  # GET /places/new
  def new
    if mobile_device?
      set_features
    end
    @place = Place.new
  end

  # GET /places/1/edit
  def edit
    if mobile_device?
      set_features
    end

    @place = Place.find(params[:id])    
    @place.business_hours.each do |k|  # So that the time populated on the form's dropdowns shows the correct time
      k.open_time = k.open_time.to_time
      k.close_time = k.close_time.to_time
    end   

  end

  # POST /places
  # POST /places.json
  def create
    auto_populated_hours
    if place_params[:business_hours_attributes]
      place_params[:business_hours_attributes].each do |k,v| # combined_time_select gem saves time in 5i format. Parsing it to correct format.
         v.parse_time_select! :open_time
         v.parse_time_select! :close_time
      end
    end
    
    @place = Place.new(place_params)
    @place.user_id = current_user.id # Save the currently logged in user's id
    @place.rating = 0.0
    @place.country = "USA" # Hardcoding to USA for now. 

    respond_to do |format|
      if @place.save
        format.html { redirect_to @place, flash: { success: 'Joey Spot was successfully created.'} }
        format.mobile { redirect_to @place, flash: { success: 'Joey Spot was successfully created.'} }
        format.json { render action: 'show', status: :created, location: @place }
      else
        format.html { render action: 'new' }
        if mobile_device?
          set_features
        end
        format.mobile { render action: 'new' }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    if place_params[:business_hours_attributes]
      place_params[:business_hours_attributes].each do |k,v|
         v.parse_time_select! :open_time
         v.parse_time_select! :close_time
      end
    end
    @place.country = "USA" # Hardcoding to USA for now.
    @place.last_edited_by = current_user.id

    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to @place, flash: { success: 'Joey Spot was successfully updated.' } }
        format.mobile { redirect_to @place, flash: { success: 'Joey Spot was successfully updated.' } }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        if mobile_device?
          set_features
        end
        format.mobile { render action: 'edit' }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url }
      format.json { head :no_content }
    end
  end

  def vote
    @place = Place.find(params[:id])
    @value = params[:value]
    vote = PlaceVote.where(:place_id => params[:id], :user_id => current_user.id).first_or_create(:value => params[:value])
    vote.update_attribute(:value, params[:value])
    respond_to do |format|
      if vote.save
        format.html { redirect_to :back, notice: "Thank you for voting." }
        format.js
        format.mobile { redirect_to :back, notice: "Your Like! has been recorded!" }
      else
        format.html { redirect_to :back, alert: "Unable to save vote" }
        format.js
        format.mobile { redirect_to :back, notice: "Your Like! has been recorded!" }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params 
      # Caching the place_params as given in http://stackoverflow.com/questions/18369592/modify-ruby-hash-in-place-rails-strong-params
      @place_params ||= params.require(:place).permit(:name, :rating, :category_id, :description, :price, :hours, :phone_num, :website,
              :images, :street, :state, :city, :zip_code, :latitude, :longitude, :event_start_date, :event_end_date, :recurrence_id, :event_price, business_hours_attributes:[:day, :open_time, :close_time, :closed, :id, :_destroy], 
              :age_group_ids =>[], :feature_ids =>[])
    end

    def auto_populated_hours
      unless (params[:open_monday] == [""])
        hash = Hash.new

        if (params[:open_monday] != ["closed"])
          hash['0'] = {:'day' => "Monday", :'open_time(5i)' => params[:open_monday], :'close_time(5i)' => params[:close_monday], :closed => 0, :id => "", :_destroy => "false"}
        else
          hash['0'] = {:'day' => "Monday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
        end
        if (params[:open_tuesday] != ["closed"])
          hash['1'] = {:'day' => "Tuesday", :'open_time(5i)' => params[:open_tuesday], :'close_time(5i)' => params[:close_tuesday], :closed => 0, :id => "", :_destroy => "false"}
        else
          hash['1'] = {:'day' => "Tuesday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
        end
        if (params[:open_wednesday] != ["closed"])
          hash['2'] = {:'day' => "Wednesday", :'open_time(5i)' => params[:open_wednesday], :'close_time(5i)' => params[:close_wednesday], :closed => 0, :id => "", :_destroy => "false"}
        else
          hash['2'] = {:'day' => "Wednesday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
        end
        if (params[:open_thursday] != ["closed"])
          hash['3'] = {:'day' => "Thursday", :'open_time(5i)' => params[:open_thursday], :'close_time(5i)' => params[:close_thursday], :closed => 0, :id => "", :_destroy => "false"}
        else
          hash['3'] = {:'day' => "Thursday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
        end
        if (params[:open_friday] != ["closed"])
          hash['4'] = {:'day' => "Friday", :'open_time(5i)' => params[:open_friday], :'close_time(5i)' => params[:close_friday], :closed => 0, :id => "", :_destroy => "false"}
        else
          hash['4'] = {:'day' => "Friday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
        end
        if (params[:open_saturday] != ["closed"])
          hash['5'] = {:'day' => "Saturday", :'open_time(5i)' => params[:open_saturday], :'close_time(5i)' => params[:close_saturday], :closed => 0, :id => "", :_destroy => "false"}
        else
          hash['5'] = {:'day' => "Saturday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
        end
        if (params[:open_sunday] != ["closed"])
          hash['6'] = {:'day' => "Sunday", :'open_time(5i)' => params[:open_sunday], :'close_time(5i)' => params[:close_sunday], :closed => 0, :id => "", :_destroy => "false"}
        else
          hash['6'] = {:'day' => "Sunday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
        end
        place_params[:business_hours_attributes] = hash
      end
    end

    def set_features
      gon.features_count = Feature.all.count
      gon.park_features = CategoriesFeature.where(category_id: 1)
      gon.museum_features = CategoriesFeature.where(category_id: 2)
      gon.restaurant_features = CategoriesFeature.where(category_id: 3)
      gon.pool_features = CategoriesFeature.where(category_id: 4)
      gon.movie_features = CategoriesFeature.where(category_id: 5)
      gon.class_features = CategoriesFeature.where(category_id: 6)
      gon.event_features = CategoriesFeature.where(category_id: 7)
      gon.retail_features = CategoriesFeature.where(category_id: 8)
      gon.school_features = CategoriesFeature.where(category_id: 9)
      gon.dentist_features = CategoriesFeature.where(category_id: 10)
      gon.amusement_features = CategoriesFeature.where(category_id: 11)
      gon.hotel_features = CategoriesFeature.where(category_id: 12)
      gon.attraction_features = CategoriesFeature.where(category_id: 13)
      gon.other_features = CategoriesFeature.where(category_id: 14)
      gon.indoor_features = CategoriesFeature.where(category_id: 15)
      gon.salon_features = CategoriesFeature.where(category_id: 16)
      gon.library_features = CategoriesFeature.where(category_id: 17)
    end

end
