class ReviewsController < ApplicationController
	before_action :authenticate_user!, only: [:edit, :update, :new]
	include ReviewsHelper

	def create
		@place = Place.find(params[:place_id])
		@review = @place.reviews.build(review_params)
		@review.user_id = current_user.id 	

		respond_to do |format|
		  if @review.save
			@place.update_attributes(rating: averageRating(@place.id))

			if params[:photo]
				@photo = @place.photos.build(params[:photo].permit(:image))
				@photo.user_id = current_user.id
				@photo.review_id = @review.id
				@photo.save
			end	

			format.mobile { redirect_to place_path(@place), flash: { success: 'Review was successfully saved.' } }
			format.html { redirect_to place_path(@place), flash: { success: 'Review was successfully saved.' } }
			format.json { render action: 'show', status: :created, location: @place }
		  else
		  	format.mobile { render action: 'new' }
			format.html { render action: 'new' }
			format.json { render json: @review.errors, status: :unprocessable_entity }
		  end
		end

	end

	# GET /places/new
	def new
		@place = Place.find(params[:place_id])
		if user_not_already_reviewed?(current_user.id, @place.id)
			@review = Review.new
		else
			@users_review = Review.where(:user_id => current_user.id).where(:place_id => @place.id)
			flash[:notice] = "You have already reviewed this place. Please edit your existing review." 
			redirect_to edit_place_review_path(@place, @users_review.first.id)
		end
	end

	def edit
	  @review = Review.find(params[:id])
	  @place = Place.find(params[:place_id])
	end

	def update
		#set the age_group_ids parameter to be an empty array if it is not passed to the update action
		#Needs this or else when unchecking all on update, it won't update.
		params[:review][:age_group_ids] ||= []  
		@place = Place.find(params[:place_id])
		@review = Review.find(params[:id])

		respond_to do |format|
		  if @review.update_attributes(review_params)
			@place.update_attributes(rating: averageRating(@place.id))

			if params[:photo]
			    @photo = @place.photos.build(params[:photo].permit(:image))
			    @photo.user_id = current_user.id
			    @photo.review_id = @review.id
			    @photo.save 
			end			

			format.html { redirect_to @place, flash: { success: 'Review was successfully updated.' } }
			format.json { render action: 'show', status: :created, location: @place }
			format.mobile { redirect_to @place, flash: { success: 'Review was successfully updated.' } }
		  else
			format.html { render action: 'edit' }
			format.json { render json: @review.errors, status: :unprocessable_entity }
			format.mobile { render action: 'edit' }
		  end
		end
	end

	def vote
		@review = Review.find(params[:id])
		@value = params[:value]
		vote = ReviewVote.where(:review_id => params[:id], :user_id => current_user.id).first_or_create(:value => params[:value])
		vote.update_attribute(:value, params[:value])
		respond_to do |format|
			if vote.save
				format.html { redirect_to :back, notice: "Thank you for voting." }
				format.js
				format.mobile { redirect_to :back, notice: "Your Thanks has been recorded!" }
			else
				format.html { redirect_to :back, alert: "Unable to save vote" }
				format.js
				format.mobile { redirect_to :back, notice: "Your Thanks has been recorded!" }
			end
		end
	end

	def review_params
		@review_params ||= params[:review].permit(:created_by, :rating, :review, :age_group_ids =>[])
	end

end
