class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
	include ApplicationHelper
	has_mobile_fu false
	before_action :prepare_for_mobile
	protect_from_forgery with: :exception
	protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/json' } 
	before_action :configure_devise_params, if: :devise_controller?
	before_action :set_cache_buster, if: :mobile_device?

	def configure_devise_params
		devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:name, :email) }
		devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation, :image) }
		devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :password_confirmation, :current_password, :image) }	      
		
	end

	def authenticate_user_from_token!
		user_email = params[:email].presence
		user       = user_email && User.find_by_email(user_email)

		if user && Devise.secure_compare(user.authentication_token, params[:auth_token])
		  sign_in user, store: false
		else
			render json: {:success=>false, :error=> "Incorrect auth token or email"}
		end
	end

	def after_sign_in_path_for(resource)
		if resource.is_a?(Admin)
			admin_dashboard_path
		else
			session["user_return_to"] || root_path
		end
	end

 	private

	def mobile_device?
		if session[:mobile_param]
			session[:mobile_param] == "1"
		else
			is_mobile_device?
		end
	end

	def prepare_for_mobile
		session[:mobile_param] = params[:mobile] if params[:mobile]
		if mobile_device?
			request.format = :mobile
		end
	end

	def set_cache_buster
		response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
		response.headers["Pragma"] = "no-cache"
		response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
	end

end
