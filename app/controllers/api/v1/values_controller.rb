module Api
	module V1
		class ValuesController < ApplicationController

			def categories
				@categories = Category.all
			end

			def features
				@categories_features = CategoriesFeature.all
			end

			def age_groups
				@age_groups = AgeGroup.all
			end

		end
	end
end