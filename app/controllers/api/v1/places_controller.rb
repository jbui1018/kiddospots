require 'search_helper'

module Api
	module V1
		class PlacesController < ApplicationController
			# before_action :authenticate_user!, only: [:update, :new, :edit]
			before_action :authenticate_user_from_token!, only: [:update, :new, :create, :edit]

			include SearchHelper
			respond_to :json

			# Catch Missing Parameter error and send JSON response
			rescue_from(ActionController::ParameterMissing) do |parameter_missing_exception|
				error = {}
				error[parameter_missing_exception.param] = ['parameter is required']
				render json: { success: false, errors: [error] } 
			end

			# Catch Invalid Parameter error and send JSON response
			rescue_from(Apipie::ParamInvalid) do |parameter_missing_exception|
				error = {}
				error[parameter_missing_exception.param] = ['parameter is invalid']
				render json: { success: false, errors: [error] } 
			end			

			# Documentation begin
			resource_description do
				formats ['json']
				api_versions "1.0"
			end

			api :GET, '/places/', "List all places or perform search"
			description <<-EOS
			Search takes the following parameters:
			* name_cont
			* city_cont
			* zip_code_eq
			* category_id_eq
			* features_id_eq_any (0 or more times)
			* price_eq

			To perform a search, do a GET request with the parameters as shown in this example:
			
			<tt>http://localhost:3000/api/places?q[name_cont]=&q[city_cont]=&q[zip_code_eq]=&q[category_id_eq]=&q[features_id_eq_any][]=&q[price_eq]=</tt> 

			An example with values supplied for some of the parameters:

			<tt>http://localhost:3000/api/places?q[name_cont]=cafe&q[city_cont]=Seattle&q[zip_code_eq]=98109&q[category_id_eq]=3&q[features_id_eq_any][]=&q[features_id_eq_any][]=15&q[features_id_eq_any][]=16&q[features_id_eq_any][]=17&q[features_id_eq_any][]=12&q[features_id_eq_any][]=18&q[features_id_eq_any][]=11&q[price_eq]=1&q[age_groups_id_eq_any][]=1&q[age_groups_id_eq_any][]=2&q[age_groups_id_eq_any][]=3&q[age_groups_id_eq_any][]=4&q[age_groups_id_eq_any][]=5
			</tt>
			EOS
			def index
					#default search distance
					defaultDistanceKm = 100
					defaultDistanceMi = 63

					@places = search_places(params, defaultDistanceKm, defaultDistanceMi)
			end

			api :GET, '/places/:id', "Get a place by id"
			param :id, :number , :desc => "id of the place", :required => true
			example "/api/places/2"
			def show
					@place = Place.find(params[:id])
			end

			def new
				@place = Place.new
			end

			api :POST, '/places', "Create a new place"
			param :email, String , :desc => "login email", :required => true
			param :auth_token, String , :desc => "auth token generated on login", :required => true
			param :place, Hash , :desc => "place object with all it's mandatory attributes" do
				param :name, String , :desc => "name of the place", :required => true
				param :category_id, Integer, :desc => "id of the category to which the place belongs", :required => true
				param :feature_ids, Array, :desc=> "ids of the features"
				param :description, String, :desc=> "description of the place", :required => true
				param :age_group_ids, Array, :desc=> "ids of age groups", :required => true
				param :price, Integer, :desc=> "price value", :required => true
				param :street, String, :desc=> "street", :required=>true
				param :city, String, :desc=> "city", :required=>true
				param :state, String, :desc =>"2 letter representation of the state. Example: WA", :required=>true
				param :zip_code, Integer, :desc=>"zip code of the place", :required=>true
			end
			description <<-EOS

			An example to create a place using curl:

			<tt>
			curl -H "Accept: application/vnd.joeysearch.v1" -H "Content-Type: application/json" </tt>
			
			<tt>
			-d '{"auth_token":"dtXZzYW3JhWkxfwgeVoi", "email":"user@gmail.com", "place":{"name":"Piroshky Piroshky", "category_id":3, "feature_ids": [17, 12, 11], "description":"Tasty", "age_group_ids":[4, 5], "price":1, "event_start_date":"", "event_end_date":"", "recurrence_id":"", "event_price":"", "street":"1908 Pike Pl", "city":"Seattle", "state":"WA", "zip_code":98101, "phone_num":"(206) 441-6068", "website":"http://www.piroshkybakery.com/"}, "open_monday":["08:00"], "close_monday":["18:00"], "open_tuesday":["08:00"], "close_tuesday":["18:00"], "open_wednesday":["08:00"], "close_wednesday":["18:00"], "open_thursday":["08:00"], "close_thursday":["18:00"], "open_friday":["08:00"], "close_friday":["18:00"], "open_saturday":["08:00"], "close_saturday":["18:30"], "open_sunday":["08:00"], "close_sunday":["18:30"]}'
			</tt>
			
			Notice how the attribues of the place are given.

			If a place is closed on a particular day, set the open_day value to "closed". For example, open_monday:"closed". No need to give close_monday.
			---
			Get back the id of the created place

			Example JSON response:
			<tt>{"place_id":11}</tt>
			EOS
			def create
				populated_hours
				if place_params[:business_hours_attributes]
					place_params[:business_hours_attributes].each do |k,v| # combined_time_select gem saves time in 5i format. Parsing it to correct format.
						 v.parse_time_select! :open_time
						 v.parse_time_select! :close_time
					end
				end
						
				@place = Place.new(place_params)
				user = User.where(:authentication_token => params[:auth_token])
				@place.user_id = user.first.id # Save the currently logged in user's id
				@place.rating = 0.0
				@place.country = "USA" # Hardcoding to USA for now. 

				if @place.save
					@place
				else
					render json: { success: false, error: @place.errors }
				end
			end

			def edit
				@place = Place.find(params[:id])
				@place.business_hours.each do |k|  # So that the time populated on the form's dropdowns shows the correct time
					k.open_time = k.open_time.to_time
					k.close_time = k.close_time.to_time
			end

			end

			api :PATCH, '/places/:id', "Edit a place"
			param :id, :number, :desc => "id of the place", :required => true
			param :email, String , :desc => "login email", :required => true
			param :auth_token, String , :desc => "auth token generated on login", :required => true
			param :place, Hash , :desc => "place object with all it's mandatory attributes" do
				param :name, String , :desc => "name of the place", :required => true
				param :category_id, Integer, :desc => "id of the category to which the place belongs", :required => true
				param :feature_ids, Array, :desc=> "ids of the features"
				param :description, String, :desc=> "description of the place", :required => true
				param :age_group_ids, Array, :desc=> "ids of age groups", :required => true
				param :price, Integer, :desc=> "price value", :required => true
				param :street, String, :desc=> "street", :required=>true
				param :city, String, :desc=> "city", :required=>true
				param :state, String, :desc =>"2 letter representation of the state. Example: WA", :required=>true
				param :zip_code, Integer, :desc=>"zip code of the place", :required=>true
				param :business_hours_attributes, Hash, :desc=>"business hours" do
					param "1", Hash, :desc=>"Monday" do
						param "open_time(5i)", String, :desc=>"open time"
						param "close_time(5i)", String, :desc=>"open time"
						param :closed, Integer, :desc=> "0 for open, 1 for closed"
						param :id, Integer, :desc=> "id of the business hour entry - returned by show place API"
						param :_destroy, String, :desc=> "\"false\" if entry is not be deleted, \"true\", if it has to be"
					end
				end
			end
			description <<-EOS

			An example to edit a place using curl:

			<tt>
			curl -H "Accept: application/vnd.joeysearch.v1" -H "Content-Type: application/json" -X PATCH </tt>
			
			<tt>
			{"auth_token":"dtXZzYW3JhWkxfwgeVoi", "email":"user@gmail.com", "place":{ "name":"Piroshky Piroshky", "category_id":3, "feature_ids":[17, 12, 11], "description":"Tasty", "age_group_ids":[4, 5], "price":1, "street":"1908 Pike Pl", "city":"Seattle", "state":"WA", "zip_code":98101, "phone_num":"(206) 441-6068", "website":"http://www.piroshkybakery.com/", "business_hours_attributes": {"1" :{"open_time(5i)":"08:00", "close_time(5i)":"18:00", "closed":0, "id":26, "_destroy":"false"}, "2" :{"open_time(5i)":"09:00", "close_time(5i)":"18:00", "closed":0, "id":27, "_destroy":"false"}, "3" :{"open_time(5i)":"09:00", "close_time(5i)":"18:00", "closed":1, "id":28, "_destroy":"false"}  }}}' http://localhost:3000/api/places/17
			</tt>
			
			Notice how the attribues of the place are given. 

			The example shows only 3 days of the week in business_hours_attributes. The rest of the days can be updated similarly. 
			---
			Get back the id of the created place

			Example JSON response:
			<tt>{"success":true,"place_id":17}</tt>
			EOS
			def update
				@place = Place.find_by_id(params[:id])
				if place_params[:business_hours_attributes]
						place_params[:business_hours_attributes].each do |k,v|
							 v.parse_time_select! :open_time
							 v.parse_time_select! :close_time
						end
				end
				@place.country = "USA" # Hardcoding to USA for now. 

				if @place.update(place_params)
					@place
				else
					render json: { success: false, error: @place.errors.full_messages }
				end

			end

			def place_params 
				@place_params ||= params.require(:place).permit(:name, :rating, :category_id, :description, :price, :hours, :phone_num, :website,
								:images, :street, :state, :city, :zip_code, :latitude, :longitude, :event_start_date, :event_end_date, :recurrence_id, 
								:event_price, business_hours_attributes:[:day, :open_time, :close_time, :closed, :id, :_destroy],
								:age_group_ids =>[], :feature_ids =>[])
			end


		def populated_hours
				unless (params[:open_monday] == [""])
				hash = Hash.new

				if (params[:open_monday] != ["closed"])
					hash['0'] = {:'day' => "Monday", :'open_time(5i)' => params[:open_monday], :'close_time(5i)' => params[:close_monday], :closed => 0, :id => "", :_destroy => "false"}
				else
					hash['0'] = {:'day' => "Monday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
				end
				if (params[:open_tuesday] != ["closed"])
					hash['1'] = {:'day' => "Tuesday", :'open_time(5i)' => params[:open_tuesday], :'close_time(5i)' => params[:close_tuesday], :closed => 0, :id => "", :_destroy => "false"}
				else
					hash['1'] = {:'day' => "Tuesday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
				end
				if (params[:open_wednesday] != ["closed"])
					hash['2'] = {:'day' => "Wednesday", :'open_time(5i)' => params[:open_wednesday], :'close_time(5i)' => params[:close_wednesday], :closed => 0, :id => "", :_destroy => "false"}
				else
					hash['2'] = {:'day' => "Wednesday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
				end
				if (params[:open_thursday] != ["closed"])
					hash['3'] = {:'day' => "Thursday", :'open_time(5i)' => params[:open_thursday], :'close_time(5i)' => params[:close_thursday], :closed => 0, :id => "", :_destroy => "false"}
				else
					hash['3'] = {:'day' => "Thursday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
				end
				if (params[:open_friday] != ["closed"])
					hash['4'] = {:'day' => "Friday", :'open_time(5i)' => params[:open_friday], :'close_time(5i)' => params[:close_friday], :closed => 0, :id => "", :_destroy => "false"}
				else
					hash['4'] = {:'day' => "Friday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
				end
				if (params[:open_saturday] != ["closed"])
					hash['5'] = {:'day' => "Saturday", :'open_time(5i)' => params[:open_saturday], :'close_time(5i)' => params[:close_saturday], :closed => 0, :id => "", :_destroy => "false"}
				else
					hash['5'] = {:'day' => "Saturday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
				end
				if (params[:open_sunday] != ["closed"])
					hash['6'] = {:'day' => "Sunday", :'open_time(5i)' => params[:open_sunday], :'close_time(5i)' => params[:close_sunday], :closed => 0, :id => "", :_destroy => "false"}
				else
					hash['6'] = {:'day' => "Sunday", :'open_time(5i)' => "00:00", :'close_time(5i)' => "00:00", :closed => 1, :id => "", :_destroy => "false"} 
				end
				place_params[:business_hours_attributes] = hash
			end
		end

		end
	end
end