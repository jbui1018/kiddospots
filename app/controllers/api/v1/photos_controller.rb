module Api
	module V1
		class PhotosController < ApplicationController

			def index
				if params[:place_id]
					place = Place.find(params[:place_id])
					@photos = place.photos
				end
			end


			api :POST, '/places/:place_id/photos/upload', "Upload a photo"
			param :email, String , :desc => "Login email", :required => true
			param :auth_token, String , :desc => "Auth token generated on login", :required => true
			param :photo, Hash , :desc => "Photo object", :required => true do
				param :description, String , :desc => "Description of the image", :required => false
				param :review_id, Integer, :desc => "ID of the review to which the photo belongs", :required => false
				param :image, Hash, :desc=>"Image object", :required=>true do
					param :content_type, String, :desc => "Image type. For examople: image/jpeg", :required => true
					param :filename, String, :desc => "Image's file name", :required => true
					param :original_filename, String, :desc => "Same as image's file name", :required => true
					param :file_data, String, :desc => "Base64 encoded image", :required => true
				end
			end
			description <<-EOS

			Example Request JSON:

			<tt>
			{ "auth_token" : "GRfYzH2Ki1y99U5znQPj", "email" : "user@gmail.com", "photo" : { "description" : "a description of the image", "review_id" : 1, "image" : {"content_type" : "image/jpeg", "filename" : "image001.JPG", "original_filename" : "image001.JPG", "file_data" : base64_encoded_string} } }
			</tt>

			Example JSON response:

			<tt>
				{"success":true,"photo_id":9,"photo":{"image":{"url":"https://kiddospots-dev.s3.amazonaws.com/uploads/photo/image/9/image.JPG","thumb":{"url":"https://kiddospots-dev.s3.amazonaws.com/uploads/photo/image/9/thumb_image.JPG"}}}}
			</tt>
			EOS


			def upload
				user = User.where(:authentication_token => params[:auth_token])

				place = Place.find(params[:place_id])

				# @photo.image.set_filename(params[:photo][:image])
				
				photo_params[:image] = parse_image_data(photo_params[:image]) if photo_params[:image]

				@photo = place.photos.build(photo_params)
				@photo.user_id = user.first.id
				
				if @photo.save
					@photo
				else
					render json: { success: false, error: @photo.errors }
				end

			ensure
				clean_tempfile
			end

			def photo_params 
				@photo_params ||= params.require(:photo).permit(:description, :review_id, image:[:content_type, :filename, :file_data])
			end

			private

			def parse_image_data(image_data)
				@tempfile = Tempfile.new('uploaded-photo')
				@tempfile.binmode
				@tempfile.write Base64.decode64(image_data[:file_data])
				@tempfile.rewind

				ActionDispatch::Http::UploadedFile.new(
					:tempfile => @tempfile,
					:content_type => image_data[:content_type],
					:filename => image_data[:filename],
					:original_filename => image_data[:original_filename]
				)
			end

			def clean_tempfile
				if @tempfile
					@tempfile.close
					@tempfile.unlink
				end
			end


		end
	end
end