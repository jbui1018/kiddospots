module Api
	module V1 

		class SessionsController < Devise::SessionsController
			before_action :authenticate_user_from_token!, :except => [:create, :destroy]
			respond_to :json

			rescue_from(FbGraph::InvalidToken) do |exception|
				error = {}
				error[exception] = ['invalid access token']
				render json: { success: false, errors: [error] } 
			end

			api :POST, '/sessions', "Login and get authentication token"
			param :email, String , :desc => "login email", :required => false
			param :password, String , :desc => "password", :required => false
			param :fb_access_token, String , :desc => "Facebook access token", :required => false
			description <<-EOS
			Get back authentication token in return.

			Example JSON response:
			<tt>{"success":true,"auth_token":"ga6zy1DZVYdJhUignRg8","name":"Foo User","id":1}</tt>
			EOS
			def create
				if params[:fb_access_token]
					user = FbGraph::User.me(params[:fb_access_token]).fetch() # Get user information from access token
					email = user.email
				else
					email = params[:email]
				end
				resource = User.find_for_database_authentication(:email => email)
				return invalid_login_attempt unless resource

				if resource.valid_password?(params[:password]) || params[:fb_access_token]
					sign_in(:user, resource)
					if params[:fb_access_token] # return email only if login is using facebook
						render :json=> {:success => true, :auth_token => resource.authentication_token, :name => resource.name, :id => resource.id, :email => resource.email }
					else
						render :json=> {:success => true, :auth_token => resource.authentication_token, :name => resource.name, :id => resource.id}
					end
					return
				end
				invalid_login_attempt
			end

			api :DELETE, '/sessions', "Logout from session"
			param :email, String , :desc => "login email", :required => true
			param :auth_token, String , :desc => "auth token generated on login", :required => true
			description <<-EOS
			Logout and delete session token.
			
			Example JSON response:
			<tt>{"success":true}</tt>
			EOS
			def destroy
				resource = User.find_for_database_authentication(:email => params[:email])
				resource.authentication_token = nil
				if resource.save
					render :json=> {:success=>true}
				else 
					render :json=> {:success=>false}
				end
			end

			protected

			def invalid_login_attempt
				render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
			end

		end

	end
end