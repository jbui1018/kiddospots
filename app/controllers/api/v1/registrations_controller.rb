module Api
	module V1 
		class RegistrationsController < Devise::RegistrationsController

			# Catch Missing Parameter error and send JSON response
			rescue_from(ActionController::ParameterMissing) do |parameter_missing_exception|
				error = {}
				error[parameter_missing_exception.param] = ['parameter is required']
				render json: { success: false, errors: [error] } 
			end

			# Catch Missing Parameter error through by apipie and send JSON response
			rescue_from(Apipie::ParamMissing) do |parameter_missing_exception|
				error = {}
				error[parameter_missing_exception.param] = ['parameter is required']
				render json: { success: false, errors: [error] } 
			end

			# Catch Invalid Parameter error and send JSON response
			rescue_from(Apipie::ParamInvalid) do |parameter_missing_exception|
				error = {}
				error[parameter_missing_exception.param] = ['parameter is invalid']
				render json: { success: false, errors: [error] } 
			end


			api :POST, '/registrations', "Register new user and get auth token"
			param :user, Hash , :desc => "hash with user registration information" do
				param :email, String , :desc => "email of the user(will be used for login)", :required => false
				param :name, String , :desc => "full name of the user", :required => false
				param :password, String , :desc => "password(minimum 6 characters)", :required => true
				param :password_confirmation, String , :desc => "password confirmation", :required => true
				param :fb_access_token, String , :desc => "Facebook access token", :required => false
			end
			description <<-EOS
			An example to register a user using curl:

			<tt>
			curl -H "Accept: application/vnd.joeysearch.v1" -H "Content-Type: application/json" -X POST http://localhost:3000/api/registrations -d '{"user":{"email":"testuser2@gmail.com","name":"Test User2","password":"test123","password_confirmation":"test123"}}'
			</tt>

			Example JSON response:

			<tt>{"success":true,"info":"Registered","data":{"name":"Test User2","auth_token":"btKY4y7F8T5wg-uh-A-m"}}</tt>
			EOS
			def create
				if params[:user][:fb_access_token]
					user_info = FbGraph::User.me(params[:user][:fb_access_token]).fetch() # Get user information from access token
					# Hacky approach - to build user info
					user = user_params.clone
					user[:uid] = user_info.raw_attributes[:id]
					user[:name] = user_info.raw_attributes[:name]
					user[:email] = user_info.raw_attributes[:email]
					user[:provider] = "facebook"
					user.delete(:fb_access_token) # The user model does not take fb_access_token. Hence deleting it. 

					build_resource(user) 
				else
					build_resource(user_params) 
				end

				if resource.save
					if params[:user][:fb_access_token] # return email only if registration is using facebook
						data = { :name => resource.name, :id =>resource.id, :auth_token=>resource.authentication_token, :email => resource.email } 
					else
						data = { :name => resource.name, :id =>resource.id, :auth_token=>resource.authentication_token } 
					end
					sign_in(resource, :store => false)
					render	:status => 200,
							:json => { :success => true,
										:info => "Registered",
										:data => data
									 }
				else
					render	:status => :unprocessable_entity,
							:json => { :success => false,
										:info => resource.errors,
										:data => {} }
				end
			end

			def update
				# For Rails 4
				account_update_params = devise_parameter_sanitizer.sanitize(:account_update)

				# required for settings form to submit when password is left blank
				if account_update_params[:password].blank?
					account_update_params.delete("password")
					account_update_params.delete("password_confirmation")
				end

				@user = User.where(:authentication_token => params[:auth_token])
				if @user.update_attributes(account_update_params)
					sign_in @user, :bypass => true
					render	:status => 200,
							:json => { :success => true,
										:info => "Updated",
										:data => { :name => @user.name, :email=>@user.email } 
											}
				else
					render	:status => :unprocessable_entity,
							:json => { :success => false,
										:info => resource.errors,
										:data => {} }
				end
			end

			def user_params
				params.require(:user).permit(:name, :email, :password, :password_confirmation, :fb_access_token)
			end

		end
	end
end