module Api
	module V1 	
		class ReviewsController < ApplicationController
			before_action :authenticate_user_from_token!, only: [:update, :new, :create, :edit]
			respond_to :json

			include ReviewsHelper

			# Catch Missing Parameter error and send JSON response
			rescue_from(ActionController::ParameterMissing) do |parameter_missing_exception|
				error = {}
				error[parameter_missing_exception.param] = ['parameter is required']
				render json: { success: false, errors: [error] } 
			end		

			# Catch Invalid Parameter error and send JSON response
			rescue_from(Apipie::ParamInvalid) do |parameter_missing_exception|
				error = {}
				error[parameter_missing_exception.param] = ['parameter is invalid']
				render json: { success: false, errors: [error] } 
			end						

			api :GET, '/places/:place_id/reviews/?page=page_number', "Get all reviews by place id"
			param :place_id, :number , :desc => "id of the place", :required => true 
			example "/api/places/2/reviews/?page=4"
			def index
				@place = Place.find(params[:place_id])
				# @reviews = @place.reviews
				@reviews = @place.reviews.paginate(page: params[:page],
																		per_page: 10)
			end


			api :GET, '/places/:place_id/reviews/:id', "Get a review"
			param :place_id, :number , :desc => "id of the place", :required => true 
			param :id, :number , :desc => "id of the review", :required => true 
			def show
				place = Place.find(params[:place_id])
				@review = place.reviews.find(params[:id])
			end


			api :POST, '/places/:place_id/reviews', "Create a review for a place"
			param :email, String , :desc => "login email", :required => true
			param :auth_token, String , :desc => "auth token generated on login", :required => true
			param :review, Hash, :desc => "Review and rating" do
				param :review, String, :desc => "the review to add", :required => true
				param :rating, Float, :desc => "rating on the scale 0 to 5", :required => true
			end
			description <<-EOS

			A curl example to create a review using curl:

			<tt>
			curl -H "Accept: application/vnd.joeysearch.v1" -H "Content-Type: application/json" -d '{"auth_token":"9ycXsJ2rcWT-gy4gdLSN", "email":"user@gmail.com", "review":{"review":"good place..blah..blah", "rating":4.5}}' http://localhost:3000/api/places/1/reviews
			</tt>
			---
			Returns true for success or error message on failure

			Example JSON response:
			<tt>{"success":true}</tt>

			If the user has already reviewed the place, failure is returned along with the previous review
			
			<tt>{"success":false,"error":"You have already reviewed this place. Please edit your existing review.","old_review":"This is my old review of the place"}
			</tt>
			EOS
			def create
				@place = Place.find(params[:place_id])
				if user_not_already_reviewed?(current_user.id, @place.id)

					@review = @place.reviews.build(review_params)
					user = User.where(:authentication_token => params[:auth_token])
					@review.user_id = user.first.id

					if @review.save
						@place.update_attributes(rating: averageRating(@place.id))
						@review
					else
						render json: { success: false, error: @review.errors } 
					end

				else
					@users_review = Review.where(:user_id => current_user.id).where(:place_id => @place.id)
					render json: {success: false, error: "You have already reviewed this place. Please edit your existing review.", old_id: @users_review.first.id, old_review: @users_review.first.review, old_rating: @users_review.first.rating}
				end
			end

			# GET /places/new
			def new
				@review = Review.new
			end


			def edit
				@review = Review.find(params[:id])
				@place = Place.find(params[:place_id])
			end


			api :PATCH, '/places/:place_id/reviews/:id', "Edit review for a place"
			param :id, :number, :desc => "id of the old review - returned by create review API on failure due to existing review", :required => true
			param :email, String , :desc => "login email", :required => true
			param :auth_token, String , :desc => "auth token generated on login", :required => true
			param :review, Hash, :desc => "Review and rating" do
				param :review, String, :desc => "the review to add", :required => true
				param :rating, Float, :desc => "rating on the scale 0 to 5", :required => true
			end
			description <<-EOS

			An example to create a review using curl:

			<tt>
			curl -H "Accept: application/vnd.joeysearch.v1" -H "Content-Type: application/json" -X PATCH -d '{"auth_token":"dtXZzYW3JhWkxfwgeVoi", "email":"user@gmail.com", "review":{"review":"ugly place", "rating":1}}' http://localhost:3000/api/places/1/reviews/207
			</tt>
			---
			Returns true for success or error message on failure

			Example JSON response:
			<tt>{"success":true,"id":207}</tt>

			where id is the id of the review
			EOS
			def update
				@place = Place.find(params[:place_id])
				@review = Review.find(params[:id])

				if @review.update_attributes(review_params)
					@place.update_attributes(rating: averageRating(@place.id))
					@review
				else
						render json: { success: false, error: @review.errors } 
				end

			end	


			api :POST, '/places/:place_id/reviews/:review_id/vote', "Vote a review"
			param :auth_token, String , :desc => "auth token generated on login", :required => true
			param :email, String , :desc => "login email", :required => true
			param :value, Integer , :desc => "vote value - 1 to thank, 0 to unthank", :required => true
			description <<-EOS

			An example to vote a review using curl:

			<tt>
			curl -H "Accept: application/vnd.joeysearch.v1" -H "Content-Type: application/json" -X POST -d '{"auth_token":"9ycXsJ2rcWT-gy4gdLSN", "email":"user@gmail.com", "value":0}' http://localhost:3000/api/places/1/reviews/2/vote
			</tt>
			---
			Returns true for success, review_id & review_votes or error message on failure

			Example JSON response:
			<tt>{"success":true,"review_id":2,"review_votes":1}</tt>
			EOS
			def vote
				user = User.where(:authentication_token => params[:auth_token])
				place = Place.find(params[:place_id])
				
				@review = place.reviews.find(params[:review_id])
				vote = ReviewVote.where(:review_id => params[:review_id], :user_id => user.first.id).first_or_create(:value => params[:value])
				

				if vote.update_attribute(:value, params[:value])
					@review
				else
					render json: { success: false, error: @review.errors } 
				end

			end

			api :POST, '/places/:place_id/reviews/user_reviewed', "Find if user has reviewed a place or not"
			param :auth_token, String , :desc => "auth token generated on login", :required => true
			param :email, String , :desc => "login email", :required => true
			param :user_id, Integer , :desc => "user id", :required => true
			description <<-EOS

			An example to vote a review using curl:

			<tt>
			curl -H "Accept: application/vnd.joeysearch.v1" -H "Content-Type: application/json" -X POST -d '{"auth_token":"_MyUZ6EKk-ER6yaxTsf1", "email":"user@hotmail.com", "user_id" : 1 }' http://localhost:3000/api/places/1/user_reviewed			</tt>
			---
			Returns review_id, review and rating if the user has already reviewed the place

			Example JSON response:
			
			<tt>{"id":1,"review":"Quis impedit voluptatem praesentium dolores excepturi vitae earum possimus et laboriosam nam hic ut rem at animi dolorem molestias et corporis necessitatibus error ipsa et nisi itaque minima voluptas tempore eligendi quaerat ut tempora molestiae ducimus recusandae quam nobis est ut et nesciunt odit quasi aut quia labore vel quo neque explicabo vero non nihil molestiae optio iste in blanditiis dolore harum quod voluptate maxime nulla aliquid beatae voluptas aut quidem repellendus ut exercitationem reiciendis consectetur est veritatis nemo incidunt dolorem voluptas dolores non voluptates placeat sit ad quas velit ab reprehenderit doloremque sit saepe similique dignissimos repudiandae et velit alias modi commodi.","rating":2.34046095540639}</tt>

			If the user has not reviewed the place, id with value 0 is returned:

			<tt>{"id":0}</tt>
			EOS
			def user_reviewed
				if user_not_already_reviewed?(params[:user_id], params[:place_id])
					render json: { id: 0 }
				else 
					@review = Review.find(params[:user_id])
				end
			end

			def review_params
				@review_params ||= params[:review].permit(:created_by, :rating, :review) 
			end

		end
	end
end