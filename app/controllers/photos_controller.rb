class PhotosController < ApplicationController
  before_action only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [ :update, :new, :edit]

  # GET /photos
  # GET /photos.json
  def index
    place = Place.find(params[:place_id])
    if mobile_device?
      @photos = place.photos.paginate(page: params[:page], per_page: 9)
    else
      @photos = place.photos
    end

    @photo = place.photos.build
    respond_to do |format|
      format.html # index.html.erb
    end

  end

  # GET /photos/1
  # GET /photos/1.json
  def show
    @place = Place.find(params[:place_id])
    @photo = @place.photos.find(params[:id])
    @first_photo = Photo.where(place_id: @photo.place_id).first
    @last_photo = Photo.where(place_id: @photo.place_id).order("id DESC").first # Read that .last is slow when there are lots of records 
    respond_to do |format|
      format.html # index.html.erb
    end      
  end

  # GET /photos/new
  def new
    place = Place.find(params[:place_id])
    @photo = place.photos.build
    respond_to do |format|
      format.html # index.html.erb
    end      
  end

  # GET /photos/1/edit
  def edit
    place = Place.find(params[:place_id])    
    @photo = place.photos.find(params[:id])
  end

  # POST /photos
  # POST /photos.json
  def create
    place = Place.find(params[:place_id])
    if params[:photo]
      respond_to do |format|
        @photo = place.photos.build(params[:photo].permit(:description, :image))
        @photo.user_id = current_user.id
        debugger
        if @photo.save
          format.html { redirect_to [@photo.place, @photo], notice: 'Photo was successfully created.' }
          format.json { render action: 'show', status: :created, location: @photo }
          format.js
          format.mobile { redirect_to place_photos_path(place), flash: { success: 'Photo was successfully created.' } }
        else
          format.html { render action: 'new' }
          format.json { render json: @photo.errors, status: :unprocessable_entity }
          format.js
          format.mobile { redirect_to place_photos_path(place), flash: { error: 'Error saving photo' } }
        end
      end
    else
      redirect_to place_photos_path(place), flash: { error: 'Error saving photo' }
    end
  end

  # PATCH/PUT /photos/1
  # PATCH/PUT /photos/1.json
  def update
    place = Place.find(params[:place_id])
    @photo = place.photos.find(params[:id])

    respond_to do |format|
      if @photo.update_attributes(params[:photo].permit(:image, :description))
        format.html { redirect_to [@photo.place, @photo], flash: { success:'Photo was successfully updated.' } }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    place = Place.find(params[:place_id])
    @photo = place.photos.find(params[:id])
    @photo.destroy

    respond_to do |format|
      format.html { redirect_to place_photos_url  }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo
      @photo = Photo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    # def photo_params
    #   params.require(:photo).permit(:place_id, :description, :image, :user_id)
    # end
end
