class EventsController < ApplicationController
  include EventsHelper
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:update, :new, :edit]
  http_basic_authenticate_with :name => "info@joeysearch.com", :password => "bingbing123", only: [:update, :new, :edit]

  # GET /events
  # GET /events.json
  def index
    if params[:date].blank?
      if params[:start_date].blank?
        @date = Date.today
        start_date = Date.today
      else
        @date = Date.parse(params[:start_date])
      end
    else
      @date = Date.parse(params[:date])
      start_date = @date
    end
    
    eventsThatDay(@date)
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.find(params[:id])

    # For GMaps Infowindow
    @map_coords_hash = Gmaps4rails.build_markers(@event) do |event, marker|
      marker.lat event.latitude
      marker.lng event.longitude
      unless mobile_device?
        marker.infowindow render_to_string(:partial => "events/infowindow", :formats => [:html, :json], :locals => { :event_obj => event })
      end
    end
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit

  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.user_id = current_user.id # Save the currently logged in user's id
    @event.country = "USA" # Hardcoding to USA for now. 

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        #format.json { render action: 'show', status: :created, location: @event }
      else
        format.html { render action: 'new' }
        #format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    @event.country = "USA" # Hardcoding to USA for now.
    @event.last_updated_by = current_user.id

    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        #format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        #format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :description, :price, :age, :street, :phone_num, :website, :user_id, :country, :state, :city, :zip_code, :slug, :latitude, :longitude, :start_date, :end_date,
       :location_name, :when, :image, event_recurrences_attributes:[:recurrence_id, :weekly_day, :monthly_type, :monthly_day, :monthly_date, :monthly_week_num])
    end
end
