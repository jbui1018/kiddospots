class WelcomeController < ApplicationController
  include EventsHelper
  def index
  	@map_coords_hash =[]
  	#Random 6 places within 8 miles. I attempted to use the "random" mysql/sqlite calls, but they
  	#were not actually being random
  	@current_city_places = Place.near(JSON.parse(cookies[:users_latlon]), 50).not_expired.starts_before(Date.today + 7).first(6)
  	
    #If not current location has been set, then we just default to Seattle
  	if @current_city_places.empty?
  		@users_city = "Seattle"
  		@current_city_places = Place.near(@users_city, 8).not_expired.starts_before(Date.today + 7).first(6)
  	end

  	@current_city_places.each do |place|
  		category = Category.find(place.category_id).name
  		@map_coords_hash << {:lat => place.latitude, :lng => place.longitude, 
  			:infowindow => render_to_string(:partial => "welcome/infowindow", :locals => { :place_obj => place }) }
  	end

  	#5 Most recent reviews
  	@latest_reviews = Review.order('created_at DESC').limit(5)
  	#5 Most recently added places
  	@latest_places = Place.order('created_at DESC').limit(5)
  	#7 Upcoming events that take place on today's date
    @upcoming_events = eventsThatDay(Date.today)
  	#4 places that are featured
  	#TODO: Change the large 30 number to something smaller once we get more data. For now, we want to make sure it
  	#always shows 4.
  	@featured_places = Place.near(JSON.parse(cookies[:users_latlon]), 30).not_expired.starts_before(Date.today + 7).where("rating >= 2.5").shuffle[0..5]
  end

  before_filter :set_city
  def set_city
    defaultDistanceMi = 100
  	#If no location is given by user and we have no saved city/latlong saved in the cookies
  	#we default to Seattle
  	if params[:location].blank? && (cookies[:users_city].blank? || cookies[:users_latlon].blank? || cookies[:users_lat].blank? || cookies[:users_lon].blank?)
  		@users_city = "Seattle"
  		cookies.permanent[:users_city] = @users_city
  		coordinates = Geocoder.coordinates(@users_city)
  		cookies.permanent[:users_latlon] = JSON.generate(coordinates)
  		cookies.permanent[:users_lat] = coordinates[0]
  		cookies.permanent[:users_lon] = coordinates[1]
  	end

  	unless params[:location].blank?

  		location = Geocoder.search(params[:location])
  		unless location.blank?
        distance = Geocoder::Calculations.distance_between(location.first.coordinates, [47.606209, -122.332071])
        if distance > defaultDistanceMi
          flash.now[:error] = 'Currently Joeysearch only supports the greater Seattle area, if the given location is within that area, make sure the city is included'
        else
    			@users_city = location.first.city
    			cookies.permanent[:users_city] = @users_city
    			cookies.permanent[:users_latlon] = JSON.generate(location.first.coordinates)
    			cookies.permanent[:users_lat] = location.first.coordinates[0]
    			cookies.permanent[:users_lon] = location.first.coordinates[1]
          flash.now[:success] = 'Location updated successfully'
        end
	  	else
        flash.now[:error] = 'Location not found'
	  		@users_city = cookies[:users_city]
	  	end
	else
		@users_city = cookies[:users_city]
  	end
  end

end
