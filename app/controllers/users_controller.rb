class UsersController < ApplicationController
  before_action :correct_user,   only: [:edit, :update]
  
  def show
    @user = User.find(params[:id])
    @reviews = @user.reviews.paginate(page: params[:page],
                                      per_page: 10)
  end
  
  def new
    @user = User.new
  end
  
  def create

  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update

  end
  
  def omniauth_failure
    flash[:error] = 'Error in Facebook log in process. Please try again'
    redirect_to signin_path
  end
  
  private

    def user_params
      params.require(:user).permit(:username, :name, :email, :password, :password_confirmation)
    end
    
    # def correct_user
    #   @user = User.find(params[:id])
    #   redirect_to(root_url) unless current_user?(@user)
    # end
end
