class EventSuggestionsController < ApplicationController
	before_action :authenticate_user!, only: [:update, :new, :edit]

	# GET /events/new
  	def new
    	@event_suggestion = EventSuggestion.new
  	end

  	def create
	    @event_suggestion = EventSuggestion.new(event_suggestion_params)
	    @event_suggestion.user_id = current_user.id # Save the currently logged in user's id
	    @event_suggestion.country = "USA" # Hardcoding to USA for now. 

	    respond_to do |format|
	      if @event_suggestion.save
	      	EventSuggestionMailer.new_event_suggestion(@event_suggestion).deliver
	        format.html { redirect_to events_path, notice: 'Thank you for submitting an event to Joeysearch. We will review the event and add it to the calendar.' }
	        #format.json { render action: 'show', status: :created, location: @event }
	      else
	        format.html { render action: 'new' }
	        #format.json { render json: @event.errors, status: :unprocessable_entity }
	      end
	    end
	end

	private
		# Never trust parameters from the scary internet, only allow the white list through.
	    def event_suggestion_params
	      params.require(:event_suggestion).permit(:name, :description, :price, :age, :street, :phone_num, :website, :user_id, :country, :state, :city, :zip_code, :start_date, :end_date,
	       :location_name, :when)
	    end
end