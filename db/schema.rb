# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140910172541) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "age_groups", force: true do |t|
    t.string   "group_name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "age_groups", ["group_name"], name: "index_age_groups_on_group_name"

  create_table "android_beta_testers", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authorizations", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "business_hours", force: true do |t|
    t.integer  "place_id"
    t.string   "day"
    t.time     "open_time"
    t.time     "close_time"
    t.boolean  "closed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["name"], name: "index_categories_on_name"

  create_table "categories_features", force: true do |t|
    t.integer  "category_id"
    t.integer  "feature_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_recurrences", force: true do |t|
    t.integer  "event_id"
    t.integer  "recurrence_id"
    t.string   "weekly_day"
    t.string   "monthly_type"
    t.integer  "monthly_date"
    t.string   "monthly_day"
    t.integer  "monthly_week_num"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_suggestions", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "price"
    t.string   "age"
    t.text     "street"
    t.string   "phone_num"
    t.string   "website"
    t.integer  "user_id"
    t.string   "country",       limit: 3
    t.string   "location_name"
    t.string   "state"
    t.string   "city"
    t.integer  "zip_code"
    t.string   "when"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "price"
    t.string   "age"
    t.text     "street"
    t.string   "phone_num"
    t.string   "website"
    t.integer  "user_id"
    t.integer  "last_updated_by"
    t.string   "country",         limit: 3
    t.string   "state"
    t.string   "city"
    t.integer  "zip_code"
    t.string   "slug"
    t.decimal  "latitude",                  precision: 10, scale: 6
    t.decimal  "longitude",                 precision: 10, scale: 6
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "location_name"
    t.string   "when"
    t.string   "image"
  end

  create_table "features", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "features", ["name"], name: "index_features_on_name"

  create_table "photos", force: true do |t|
    t.integer  "place_id"
    t.string   "description"
    t.string   "image"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "review_id"
  end

  add_index "photos", ["place_id"], name: "index_photos_on_place_id"

  create_table "place_votes", force: true do |t|
    t.integer  "place_id"
    t.integer  "user_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "places", force: true do |t|
    t.string   "name"
    t.float    "rating"
    t.integer  "category_id"
    t.text     "description"
    t.integer  "price"
    t.text     "street"
    t.string   "phone_num"
    t.string   "website"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "last_edited_by"
    t.string   "country",          limit: 3
    t.string   "state"
    t.string   "city"
    t.integer  "zip_code"
    t.string   "slug"
    t.decimal  "latitude",                   precision: 10, scale: 6
    t.decimal  "longitude",                  precision: 10, scale: 6
    t.date     "event_start_date"
    t.date     "event_end_date"
    t.decimal  "event_price"
    t.integer  "recurrence_id"
  end

  add_index "places", ["slug"], name: "index_places_on_slug"
  add_index "places", ["user_id", "created_at"], name: "index_places_on_user_id_and_created_at"

  create_table "places_age_groups", force: true do |t|
    t.integer  "place_id"
    t.integer  "age_group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "places_age_groups", ["place_id"], name: "index_places_age_groups_on_place_id"

  create_table "places_features", force: true do |t|
    t.integer  "place_id"
    t.integer  "feature_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "places_features", ["place_id"], name: "index_places_features_on_place_id"

  create_table "recurrences", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "review_votes", force: true do |t|
    t.integer  "review_id"
    t.integer  "user_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reviews", force: true do |t|
    t.integer  "user_id"
    t.text     "review"
    t.float    "rating"
    t.integer  "place_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reviews", ["place_id", "created_at"], name: "index_reviews_on_place_id_and_created_at"

  create_table "reviews_age_groups", force: true do |t|
    t.integer "review_id"
    t.integer "age_group_id"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "provider"
    t.string   "uid"
    t.string   "authentication_token"
    t.string   "image"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["slug"], name: "index_users_on_slug"
  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
