# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
# 1.Slides, 2.Swings, 3.Monkey Bars, 4.Sandbox, 5.Birthday Party Venue, 6.Baseball Field
# 7.Toddler Playset, 8.Soccer Field, 9.Skate Park, 10.Basketball Court, 11.Diaper Changing Station, 
# 12.Kids Play Area, 13.Kids Activities, 14.Stroller Rental, 15.Kids Menu, 16.High Chair,
# 17.Carseat Bassinet, 18.Coloring Supplies, 19.Kids Pool, 20.Kids Beach Area, 21.Water Slides,
# 22.Cry Room, 23.Kid Friendly Viewings, 24.Kids Items, 25.Specialized for Kids, 26.Life Jackets,
# 27.Toddler Open Gym, 28.Parent Co-op, 29.Montesorri, 30.Waldorf, 31.Reggio Emilia, 32.Highscope,
# 33.Bank Street, 34.Full Day, 35.Half Day, 36.Potty Training Required, 37.Bathroom Available (park),
# 38.Splash Park, 39.Petting Zoo, 40.Booster Seats, 41.Food for Purchase, 42.Trails, 43.Stroller Friendly,
# 44.Family Changing Room, 45.Kid's Cups, 46.Picnic Tables, 47.Happy Hour Outside the Bar, 48.Large Family Tables
# 49.Drop-In, 50.Trampoline Park, 51.Bouncy Houses, 52.Play-Based(Preschool), 53.Language(Preschool),
# 54.Religiously Affiliated, 55.Ice Skating, 56.Roller Skating, 57.Arcade, 58.Swimming, 59.Music, 60.Dance/Ballet,
# 61.Martial Arts, 62.Sports League, 63.Parent-Child, 64.Bowling, 65.Gymnastics, 66.Drive Thru, 67.Story Time,
# 68.Arts and Crafts 69.School Field Trip Programs


#If you add any features_list, add it to the bottom of the list. This order is vital to the categories_list
features_list = { 
					"Slides" => [], "Swings" => [], "Monkey Bars" => [], "Sandbox" => [], "Birthday Party Venue" => [], "Baseball Field" => [], "Toddler Playset" => [], "Soccer Field" => [], "Skate Park" => [],
					"Basketball Court" => [], "Diaper Changing Table / Station" => [], "Kids Play Area" => [], "Kids Activities" => [], "Stroller Rental" => [], "Kids Menu" => [], "High Chairs" => [],
					"Carseat Bassinet" => [], "Coloring Supplies" => [], "Kids Pool" => [], "Kids Beach Area" => [], "Water Slides" => [], "Cry Room" => [], "Kid Friendly Viewings" => [],
					"Kid's Items" => [], "Specialized for Kids" => [], "Life Jackets" => [], "Toddler Open Gym" => [], "Parent Co-op" => [], "Montessori" => [], "Waldorf" => [], "Reggio Emilia" => [],
					"HighScope" => [], "Bank Street" => [], "Full Day" => [], "Half Day" => [], "Potty Training Required" => [], "Bathroom Available" => [], "Splash Park" => [], "Petting Zoo" => [],
					"Booster Seats" => [], "Food for Purchase" => [], "Trails" => [], "Stroller Friendly" => [], "Family Changing Room" => [], "Kids Cups" => [], "Picnic Tables" => [],
					"Happy Hour Outside the Bar" => [], "Large Family Tables" => [], "Drop-In" => [], "Trampoline Park" => [], "Bouncy Houses" => [], "Play-Based" => [], "Language" => [],
					"Religiously Affiliated" => [], "Ice Skating" => [], "Roller Skating" => [], "Arcade" => [], "Swimming" => [], "Music" => [], "Dance/Ballet" => [], "Martial Arts" => [],
					"Team Sports" => [], "Parent-Child" => [], "Bowling" => [], "Gymnastics" => [], "Drive Thru" => [], "Story Time" => [], "Arts and Crafts" => [], "School Field Trip Programs" => [],
				}
categories_list = {
					"Parks/Playgrounds/Beaches/Hikes" => ["1", "2", "3", "4", "7", "6", "8", "9", "10", "42", "46", "43", "11", "37", "19", "20", "38", "39", "41"], 
					"Museums/Aquariums/Zoos/Farms" => ["12", "13", "14", "11", "5", "69", "41", "43"], 
					"Restaurants/Cafes" => ["15", "16", "40", "17", "12", "18", "11", "43", "45", "47", "48", "66", "5"], 
					"Pools/Recreation/Community Centers" => ["19", "20", "21", "26", "27", "11", "41", "44", "5", "69"],
					"Theaters" => ["22", "23", "11", "5", "69"], 
					"Classes/Lessons/Leagues/Camps" => ["58", "59", "60", "61", "62", "63", "65", "68", "5", "69"],
					"Events" =>[], 
					"Retail/Grocery Stores/Malls" => ["25", "24", "12", "11", "41", "5", "69"],
					"Schools/Day Care" => ["28", "29", "30", "31", "32", "33", "52", "53", "54", "49", "34", "35", "36", "5"],
					"Dentists/Physicians/Health" => ["25","12"],
					"Amusement Parks" => ["11", "13", "14", "41", "43", "5", "69"],
					"Hotel/Accommodations" => ["12", "13", "19"],
					"Attractions" => ["69", "41"],
					"Other" => [],
					"Indoor Activity Center" => ["50", "51", "55", "56", "57", "64", "68", "12", "25", "5", "41", "11", "69"],
					"Salons/Barber Shops" => ["25", "12"],
					"Libraries/Book Stores" => ["67", "12", "11", "41", "5", "69"],
				}

age_groups = { "Infants" => "0-1", "Toddlers" => "1-2", "Preschooler" => "3-6", 
				"Early Elementary" => "7-9", "Elementary" => "10-12"}

recurrences = { "Once" => [], "Daily" => [], "Weekly" => [], "Monthly" => []}

categories = categories_list.keys

if Feature.count == 0
	puts "Seeding features"
	features_list.each do | feature_name, desc | 
		Feature.create(name:feature_name)	
	end	
end

if Category.count == 0
	puts "Seeding categories"
	categories.each_with_index do | category_name, id | 
		Category.create(name:category_name)
		categories_list[category_name].each do | feature_id |
			CategoriesFeature.create(category_id:id+1, feature_id:feature_id)
		end
	end	
end

if AgeGroup.count == 0
	puts "Seeding Age Groups..."
	age_groups.each do | group, desc | 
		AgeGroup.create(group_name: group, description: desc)
	end
end

if Recurrence.count == 0
	puts "Seeding Recurrence..."
	recurrences.each do | name, desc |
		Recurrence.create(name:name)
	end
end

puts "Seeding Done!"