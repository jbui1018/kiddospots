class AddInfantToddlerPreschoolEarlyElementaryToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :infant, :integer
    add_column :reviews, :toddler, :integer
    add_column :reviews, :preschool, :integer
    add_column :reviews, :early, :integer
    add_column :reviews, :elementary, :integer
  end
end
