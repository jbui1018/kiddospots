class CreatePlacesAgeGroups < ActiveRecord::Migration
  def change
    create_table :places_age_groups do |t|
      t.integer :place_id
      t.integer :age_group_id

      t.timestamps
    end
    add_index :places_age_groups, :place_id
  end
end
