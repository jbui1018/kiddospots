class CreatePlaceVotes < ActiveRecord::Migration
  def change
    create_table :place_votes do |t|
      t.integer :place_id
      t.integer :user_id
      t.integer :value

      t.timestamps
    end
  end
end
