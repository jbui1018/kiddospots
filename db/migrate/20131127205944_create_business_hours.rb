class CreateBusinessHours < ActiveRecord::Migration
  def change
    create_table :business_hours do |t|
      t.integer :place_id
      t.string :day
      t.time :open_time
      t.time :close_time
      t.boolean :closed

      t.timestamps
    end
  end
end
