class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.string :price
      t.string :age
      t.text :street
      t.string :phone_num
      t.string :website
      t.integer :user_id
      t.integer :last_updated_by
      t.string :country, limit: 3
      t.string :state
      t.string :city
      t.integer :zip_code
      t.string :slug
      t.decimal :latitude, precision: 10, scale: 6
      t.decimal :longitude, precision: 10, scale: 6
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
