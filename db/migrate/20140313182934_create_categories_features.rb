class CreateCategoriesFeatures < ActiveRecord::Migration
  def change
    create_table :categories_features do |t|
      t.integer :category_id
      t.integer :feature_id

      t.timestamps
    end
  end
end
