class RemoveInfantToddlerPreschoolEarlyElementaryFromReview < ActiveRecord::Migration
  def change
    remove_column :reviews, :infant, :integer
    remove_column :reviews, :toddler, :integer
    remove_column :reviews, :preschool, :integer
    remove_column :reviews, :early, :integer
    remove_column :reviews, :elementary, :integer
  end
end
