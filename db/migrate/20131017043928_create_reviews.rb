class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :user_id
      t.text :body
      t.float :rating
      t.references :place

      t.timestamps
    end
    add_index :reviews, [:place_id, :created_at]
  end
end
