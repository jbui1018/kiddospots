class CreateAgeGroups < ActiveRecord::Migration
  def change
    create_table :age_groups do |t|
      t.string :group_name
      t.string :description

      t.timestamps
    end
    add_index :age_groups, :group_name
  end
end
