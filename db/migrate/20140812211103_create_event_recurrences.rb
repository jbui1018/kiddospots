class CreateEventRecurrences < ActiveRecord::Migration
  def change
    create_table :event_recurrences do |t|
      t.integer :event_id
      t.integer :recurrence_id
      t.string :weekly_day
      t.string :monthly_type
      t.integer :monthly_date
      t.string :monthly_day
      t.integer :monthly_week_num

      t.timestamps
    end
  end
end
