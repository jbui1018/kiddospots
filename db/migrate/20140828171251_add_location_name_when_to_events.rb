class AddLocationNameWhenToEvents < ActiveRecord::Migration
  def change
    add_column :events, :location_name, :string
    add_column :events, :when, :string
  end
end
