class AddColumnsToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :last_edited_by, :integer
    add_column :places, :country, :string, limit: 3
    add_column :places, :state, :string
    add_column :places, :city, :string
    add_column :places, :zip_code, :integer
  end
end
