class CreatePlacesFeatures < ActiveRecord::Migration
  def change
    create_table :places_features do |t|
      t.integer :place_id
      t.integer :feature_id

      t.timestamps
    end
    add_index :places_features, :place_id
  end
end
