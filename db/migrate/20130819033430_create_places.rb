class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name, :length => 50
      t.float :rating
      t.integer :category_id
      t.text :description, :length => 150
      t.integer :price
      t.text :street
      t.string :phone_num
      t.string :website

      t.integer :user_id

      t.timestamps
    end
    add_index :places, [:user_id, :created_at]
  end
end
