class CreateReviewsAgeGroups < ActiveRecord::Migration
  def change
    create_table :reviews_age_groups do |t|
      t.integer :review_id
      t.integer :age_group_id
    end
  end
end
