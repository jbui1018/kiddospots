class CreateEventSuggestions < ActiveRecord::Migration
  def change
    create_table :event_suggestions do |t|
      t.string :name
      t.text :description
      t.string :price
      t.string :age
      t.text :street
      t.string :phone_num
      t.string :website
      t.integer :user_id
      t.string :country, limit: 3
      t.string :location_name
      t.string :state
      t.string :city
      t.integer :zip_code
      t.string :when
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
