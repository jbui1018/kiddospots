class AddEventsToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :event_start_date, :date
    add_column :places, :event_end_date, :date
    add_column :places, :event_price, :decimal
    add_column :places, :recurrence_id, :integer
  end
end
